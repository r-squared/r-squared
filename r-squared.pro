#-------------------------------------------------
#
# Project created by QtCreator 2014-03-09T22:38:56
#
#-------------------------------------------------

## -*- mode: Makefile; c-indent-level: 4; c-basic-offset: 4;  tab-width: 8; -*-
##
## Qt usage example for RInside, inspired by the standard 'density
## sliders' example for other GUI toolkits
##
## This file can be used across operating systems as qmake selects appropriate
## values as needed, as do the R and R-related calls below. See the thread at
##     http://thread.gmane.org/gmane.comp.lang.r.rcpp/4376/focus=4402
## for discussion specific to Windows.
##
## Copyright (C) 2012  Dirk Eddelbuettel and Romain Francois

QT += core gui printsupport webenginewidgets #webkitwidgets

#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = r-squared-project
TEMPLATE = app

target.path = /usr/bin
INSTALLS += target

data.path = /usr/share/r-squared-project/data
data.files = data/*
INSTALLS += data


SOURCES += main.cpp\
        mainwindow.cpp \
    editor.cpp \
    files.cpp \
    datagrid.cpp \
    statistics.cpp \
    generic.cpp

HEADERS  += mainwindow.h \
    editor.h \
    files.h \
    datagrid.h \
    statistics.h \
    generic.h

RESOURCES += \
    icons.qrc


TRANSLATIONS = r-squared-project_es.ts  r-squared-project_en.ts
CODECFORTR  = UTF-8
CODECFORSRC = UTF-8


## comment this out if you need a different version of R,
## and set set R_HOME accordingly as an environment variable
R_HOME = 		$$system(R RHOME)
#message("R_HOME is" $$R_HOME)

## include headers and libraries for R
RCPPFLAGS = 		$$system($$R_HOME/bin/R CMD config --cppflags)
RLDFLAGS = 		$$system($$R_HOME/bin/R CMD config --ldflags)
RBLAS = 		$$system($$R_HOME/bin/R CMD config BLAS_LIBS)
RLAPACK = 		$$system($$R_HOME/bin/R CMD config LAPACK_LIBS)

## if you need to set an rpath to R itself, also uncomment
RRPATH =		-Wl,-rpath,$$R_HOME/lib

## include headers and libraries for Rcpp interface classes
## note that RCPPLIBS will be empty with Rcpp (>= 0.11.0) and can be omitted
RCPPINCL = 		$$system($$R_HOME/bin/Rscript -e \"Rcpp:::CxxFlags\(\)\")
RCPPLIBS = 		$$system($$R_HOME/bin/Rscript -e \"Rcpp:::LdFlags\(\)\")

## for some reason when building with Qt we get this each time
##   /usr/local/lib/R/site-library/Rcpp/include/Rcpp/module/Module_generated_ctor_signature.h:25: warning: unused parameter ‘classname
## so we turn unused parameter warnings off
## no longer needed with Rcpp 0.9.3 or later
RCPPWARNING =		-Wno-unused-parameter

## include headers and libraries for RInside embedding classes
RINSIDEINCL = 		$$system($$R_HOME/bin/Rscript -e \"RInside:::CxxFlags\(\)\")
RINSIDELIBS = 		$$system($$R_HOME/bin/Rscript -e \"RInside:::LdFlags\(\)\")

## compiler etc settings used in default make rules
QMAKE_CXXFLAGS +=	$$RCPPWARNING $$RCPPFLAGS $$RCPPINCL $$RINSIDEINCL
QMAKE_LIBS +=           $$RLDFLAGS $$RBLAS $$RLAPACK $$RINSIDELIBS $$RCPPLIBS

## addition clean targets
QMAKE_CLEAN +=		r-squared-project Makefile

copydata.commands = $(COPY_DIR) $$PWD/json $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata

OTHER_FILES += \
    qss/style.css
