# RSquared Project

#### Description

**R² project** is a **GUI** (graphical user interface) for [R](https://www.r-project.org/) aimed at facilitating the teaching of **Statistics at University**.

#### Official site

[RSquared project (official site)](https://rsquaredproject.wordpress.com/)

#### Developers

* Eduardo Nacimiento García (Computer engineer)
* Andrés Nacimiento García (Computer engineer)

#### Contributors

* Holi Díaz Kaas (Translations)

#### I want to be part of this project ...

This is a **free software** project, and if you want to participate, just follow the instructions described in our [wiki](https://gitlab.com/r-squared/r-squared/wikis/home)

#### Awards and prizes

* **Winner** of VIII Free Software University Contest of the [University of La Laguna](http://ull.es/)