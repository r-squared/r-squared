/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef STATISTICS_H
#define STATISTICS_H

#include "mainwindow.h"
#include "datagrid.h"
#include "generic.h"


class MainWindow;
class DataGrid;

class Statistics {

public:

    Statistics(MainWindow *w); // Constructor
    ~Statistics(); // Destructor
    void genericFunction (QString operation, QMap<QString, RFunctions> *rf);
    void summary ();
    void unidimensional (QString operation);
    void unidimensionalGraph (QString operation);
    void bidimensional (QString operation);
    void bidimensionalGraph (QString operation);
    void fittingModels (QString operation);
    void transformed(QString operation);
    void anova();
    void boxCox();
    void curveHist();
    void shapiroWilk();
    void kolmogorovSmirnov();
    void durbinWatson();
    void qqPlot(QString operation);

    void timeSeries();
    void timeSeriesDiff();
    void autocorrelations();
    void arima();

    QString allModels(bool dialog, QString type);

private:
    MainWindow *w_;
    DataGrid *dg_;
};

#endif // STATISTICS_H
