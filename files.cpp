/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "files.h"

// Constructor
Files::Files(MainWindow *w) {
    w_ = w;
    reloadSources();
}

// Destructor
Files::~Files() {

}

// OPEN FILE. Open files with .csv format
void Files::openFile() {

    w_->edit_ = false;
    QString fileName;
    fileName = QFileDialog::getOpenFileName(w_,
                                                 QObject::tr("Open file CSV"),
                                                 "",
                                                 QObject::tr("Files csv (*.csv)"));
    if (fileName != "") {
        // Clear all data
        newProject();

        // Start: read data
        SEXP ans;
        std::string fileNameStd = fileName.toStdString();
        ans = w_->R_.parseEval("mydata = read.csv(\""+fileNameStd+"\", header=TRUE, sep=\",\")");
        Rcpp::DataFrame DF(ans);

        QVector<Rcpp::DoubleVector> dataVector;

        ans = w_->R_.parseEval("names(mydata)");
        Rcpp::CharacterVector names(ans);

        int numrows = DF.nrows();

        if (numrows > w_->totalRows_) {
            int i;
            int tmpRows = w_->totalRows_;
            for (i = tmpRows; i <= numrows; i++) {
                w_->gridMain_->insertRow(i);
                w_->totalRows_++;
            }
        }

        for (int i = 0; i < DF.size(); i++) {
            w_->totalCols_ = w_->totalCols_ + 1;
            Rcpp::DoubleVector tmpDF = DF[i];
            dataVector.append(tmpDF);
            w_->listHeader_ << (QString)names[i];

            for (int row = 0; row < dataVector[i].size(); row++) {
                w_->gridMain_->setItem(row, i, new QTableWidgetItem (QString::number(dataVector[i][row])));
            }
        }
        w_->gridMain_->setHorizontalHeaderLabels(w_->listHeader_);
    }

   w_->tabs_->setCurrentWidget(w_->wgtTabs_);
   w_->edit_ = true;
}

// LOAD WORKSPACE. Load previous workspace saved
void Files::loadWorkspace() {

    w_->edit_ = false;
    QString fileName;
    fileName = QFileDialog::getOpenFileName(w_,
                                                 QObject::tr("Open workspace"),
                                                 "",
                                                 QObject::tr("Files RData (*.RData)"));
    if (fileName != "") {
        // Clear all data
        newProject();

        // Start: read data
        w_->R_.parseEvalQ("load(\""+fileName.toStdString()+"\")");

        SEXP ans;
        ans = w_->R_.parseEval("mydata");
        Rcpp::DataFrame DF(ans);

        QVector<Rcpp::DoubleVector> dataVector;

        ans = w_->R_.parseEval("names(mydata)");
        Rcpp::CharacterVector names(ans);

        int numrows = DF.nrows();

        if (numrows > 25) {
            for (int i = 25; i <= numrows; i++) {
                w_->gridMain_->insertRow(i);
            }
        }

        for (int i = 0; i < DF.size(); i++) {
            w_->totalCols_++;
            Rcpp::DoubleVector tmpDF = DF[i];
            dataVector.append(tmpDF);
            w_->listHeader_ << (QString)names[i];

            for (int row = 0; row < dataVector[i].size(); row++) {
                w_->gridMain_->setItem(row, i, new QTableWidgetItem (QString::number(dataVector[i][row])));
            }
        }
        w_->gridMain_->setHorizontalHeaderLabels(w_->listHeader_);


    }
    w_->edit_ = true;
}

// SAVE WORKSPACE. Save the actual workspace you are using
void Files::saveWorkspace() {

    QString fileName;
    fileName = QFileDialog::getSaveFileName(w_,
                                                 QObject::tr("Save workspace"),
                                                 "",
                                                 QObject::tr("Files RData (*.RData)"));
    if (fileName != "") {
        if (QFileInfo(fileName).suffix().isEmpty()) {
                    fileName.append(".RData");
        }
        w_->R_.parseEvalQ("save.image(file = \""+fileName.toStdString()+"\")");
    }

}


// NEW FILE
void Files::newProject() {
    // Clear all data
    w_->gridMain_->clear();
    w_->R_.parseEvalQ("rm(list=ls())");
    w_->totalCols_ = 0;
    w_->listHeader_.clear();
}



// NEW FILE
void Files::reloadSources() {
    w_->R_.parseEvalQ("sourceDirectory(\"Rlocal/\", recursive=TRUE, modifiedOnly=TRUE);");
    //w_->R_.parseEvalQ("sourceDirectory(\"Rlocal/\", recursive=TRUE);");

}



// Load JSON configurations
void Files::loadFileConfiguration() {
    QMessageBox msgBox(w_);

    msgBox.addButton(QMessageBox::Ok);
    msgBox.setText(QObject::tr("<b>Configuration Loader</b></br> <br/>"
                      "You can load JSON files with the configuration and add more features to <strong>R-Squared</strong>"
                      "<br/><br/><br/>"
                      "You can load multiple files at once"));

    msgBox.setIconPixmap(QPixmap(":/images/images/logo_mini.png"));
    msgBox.setWindowTitle("Configuration Loader");
    msgBox.exec();
    QStringList fileName = QFileDialog::getOpenFileNames(w_, QObject::tr("Select one or more files to load"), QString(),
            QObject::tr("JSON Files (*.json)"));

    QDir* dir = new QDir;
    if (!dir->exists("json/user")) {
        dir->mkpath("json/user");
    }

    if (fileName.empty()) {
        return;
    }

    QString content = "<strong>The configuration has been loaded from this files:</strong>";
    content += "<ul>";
    foreach (QString fname, fileName ) {
        QFileInfo* fileInfoJSON = new QFileInfo(fname);
        QFile* fileJSON = new QFile(fname);
        bool fl_copy = fileJSON->copy(fname, "json/user/"+fileInfoJSON->fileName());
        if (fl_copy) {
            content += "<li>" + fileInfoJSON->fileName() + " <strong style=\"color: green;\"> Done</strong></li>";
        }
        else {
            content += "<li>" + fileInfoJSON->fileName() + " <strong style=\"color: red;\"> Fail</strong></li>";
        }
    }
    content += "</ul>";

    msgBox.setText(content);


    msgBox.setIconPixmap(QPixmap(":/images/images/logo_mini.png"));
    msgBox.setWindowTitle("Configuration Loader");
    msgBox.exec();
}


// Load JSON configurations
void Files::loadFileFunctionsR() {
    QMessageBox msgBox(w_);

    msgBox.addButton(QMessageBox::Ok);
    msgBox.setText(QObject::tr("<b>R functions Loader</b></br> <br/>"
                      "You can load R functions files with the configuration and add more features to <strong>R-Squared</strong>"
                      "<br/><br/><br/>"
                      "You can load multiple files at once"));

    msgBox.setIconPixmap(QPixmap(":/images/images/logo_mini.png"));
    msgBox.setWindowTitle("R functions Loader");
    msgBox.exec();
    QStringList fileName = QFileDialog::getOpenFileNames(w_, QObject::tr("Select one or more files to load"), QString(),
            QObject::tr("R Files (*.r)"));

    QDir* dir = new QDir;
    if (!dir->exists("Rlocal/user")) {
        dir->mkpath("Rlocal/user");
    }

    if (fileName.empty()) {
        return;
    }

    QString content = "<strong>The configuration has been loaded from this files:</strong>";
    content += "<ul>";
    foreach (QString fname, fileName ) {
        QFileInfo* fileInfoR = new QFileInfo(fname);
        QFile* fileR = new QFile(fname);
        bool fl_copy = fileR->copy(fname, "Rlocal/user/"+fileInfoR->fileName());
        if (fl_copy) {
            content += "<li>" + fileInfoR->fileName() + " <strong style=\"color: green;\"> Done</strong></li>";
        }
        else {
            content += "<li>" + fileInfoR->fileName() + " <strong style=\"color: red;\"> Fail</strong></li>";
        }
    }
    content += "</ul>";

    msgBox.setText(content);


    msgBox.setIconPixmap(QPixmap(":/images/images/logo_mini.png"));
    msgBox.setWindowTitle("R functions Loader");
    msgBox.exec();
}
