/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GENERIC_H
#define GENERIC_H


#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonArray>
#include <QJsonObject>
#include <QVector>
#include <QMap>
#include <QFile>
#include <QMapIterator>
#include <QMessageBox>
#include <QSpinBox>
#include <QDirIterator>

class RArguments {
    public:
        RArguments(); // Constructor
        ~RArguments(); // Destructor

        QString value;
        QString optional;
        QString type;
        QString isdefault;
        QString about;
};


class RFunctions {
    public:
        RFunctions(); // Constructor
        ~RFunctions(); // Destructor

        QString name;
        QString title;
        QString description;
        QString menu;
        QString category;
        QString output;  // Text or graphs
        QVector<RArguments> arguments;
};


class Generic {
    public:
        Generic(); // Constructor
        ~Generic(); // Destructor
        void readConfiguration();
        void createConfiguration();
        QMap<QString, RFunctions>* rf_;
};



#endif // GENERIC_H
