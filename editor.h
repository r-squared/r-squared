/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef EDITOR_H
#define EDITOR_H

#include <QWidget>
#include <QTextEdit>
#include <QGridLayout>
#include <QToolBar>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QFile>
#include <QFileDialog>
#include <QPrinter>
#include <QComboBox>
#include <QFontComboBox>
#include <QFontDialog>
#include <QInputDialog>
#include <QImage>
#include <QImageReader>
#include <QUrl>
#include <RInside.h>

class editor : public QWidget
{
    Q_OBJECT
public:
    explicit editor(QWidget *parent = 0);
    QTextEdit* getTxtEditor();
    QTextEdit* txtEditor_;



signals:

public slots:
    void saveDocument();
private slots:
    void createTableToolBar();
    void setTextBold();
    void setTextItalic();
    void setTextUnderline();
    void setTextCenter();
    void setTextLeft();
    void setTextRight();
    void setTextJustify();
    void openDocument();
    void saveDocumentPdf();
    void setFontCombo(const QFont &font);
    void setTextSize(const QString &size_s);
    void changeFont();
    void insertTable();
    void insertImage();

protected:


private:

    QWidget* wgtEditor_;
    QGridLayout* lytEditor_;

    QToolBar* editorToolBar_;

    QAction     *actFileOpen_,
                *actFileSave_,
                *actFilePdf_,
                *actEditCopy_,
                *actEditPaste_,
                *actEditCut_,
                *actEditUndo_,
                *actEditRedo_,
                *actInsertTable_,
                *actInsertImage_,
                *actFormatFont_,
                *actFormatBold_,
                *actFormatItalic_,
                *actFormatUnderline_,
                *actFormatCenter_,
                *actFormatLeft_,
                *actFormatRight_,
                *actFormatJustify_;

    QComboBox *actFormatSize_;
    QFontComboBox *actFormatFontCombo_;

};

#endif // EDITOR_H
