/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "generic.h"

Generic::Generic() {
    rf_ = new QMap<QString, RFunctions>;
}

// Destructor
Generic::~Generic() {}

void Generic::readConfiguration() {
    QString dir = QDir::currentPath() + QString("/json");

    QDirIterator it(dir, QStringList() << "*.json", QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext()) {
        QFile jsonFile(it.next());
        jsonFile.open(QFile::ReadOnly);
        QJsonDocument doc =  QJsonDocument().fromJson(jsonFile.readAll());
        QJsonObject jsonObject = doc.object();

        QJsonArray JSONfunction = jsonObject.value("function").toArray();
        foreach (const QJsonValue & valueF, JSONfunction) {
            RFunctions rfTmp;
            QJsonObject objF = valueF.toObject();
            rfTmp.name = objF["name"].toString();
            rfTmp.title = objF["title"].toString();
            rfTmp.description = objF["description"].toString();
            rfTmp.menu = objF["menu"].toString();
            rfTmp.category = objF["category"].toString();
            rfTmp.output = objF["output"].toString();

            QJsonArray argument = objF["argument"].toArray();
            foreach (const QJsonValue & value, argument) {
                RArguments raTmp;
                QJsonObject obj = value.toObject();
                raTmp.value = obj["value"].toString();
                raTmp.optional = obj["optional"].toString();
                raTmp.type =  obj["type"].toString();
                raTmp.isdefault = obj["default"].toString();
                raTmp.about =  obj["about"].toString();
                rfTmp.arguments.push_back(raTmp);
            }
            rf_->insert(objF["name"].toString(), rfTmp);
        }
        jsonFile.close();
    }
}


// Create Json configuration
void Generic::createConfiguration() {
}


// Constructor
RFunctions::RFunctions() {
}

// Destructor
RFunctions::~RFunctions() {}


// Constructor
RArguments::RArguments() {
}

// Destructor
RArguments::~RArguments() {}

