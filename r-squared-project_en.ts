<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="44"/>
        <source>R-squared</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="62"/>
        <source>Data grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="63"/>
        <source>Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="105"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="107"/>
        <source>&amp;Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="109"/>
        <source>&amp;Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>&amp;New Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="124"/>
        <source>&amp;Open CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="128"/>
        <source>&amp;Open RData</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="132"/>
        <source>&amp;Save RData</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="136"/>
        <source>&amp;Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="142"/>
        <source>&amp;New Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="146"/>
        <source>&amp;Edit cell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="150"/>
        <source>&amp;Rename var</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>&amp;Unidimensional</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <source>&amp;Bidimensional</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="161"/>
        <source>&amp;Fitting models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>&amp;Transformed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="167"/>
        <source>&amp;Time Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="174"/>
        <source>Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="192"/>
        <source>max|Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="194"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="198"/>
        <source>min|Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="200"/>
        <source>Sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="204"/>
        <source>sum|Sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="206"/>
        <source>Cumulative Sums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="210"/>
        <source>cumsum|Cumulative Sums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <source>Cumulative Products</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="216"/>
        <source>cumprod|Cumulative Products</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="218"/>
        <source>Cumulative Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="222"/>
        <source>cummax|Cumulative Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="224"/>
        <source>Cumulative Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="228"/>
        <source>cummin|Cumulative Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="230"/>
        <source>Median absolute deviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="234"/>
        <source>mad|Median absolute deviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="236"/>
        <source>Mean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="240"/>
        <source>mean|Mean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <source>Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="246"/>
        <source>median|Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="248"/>
        <source>Standard deviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="252"/>
        <source>sd|Standard Deviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="254"/>
        <source>Variance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="258"/>
        <source>var|Variance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <source>hist|Histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="268"/>
        <source>Box Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="272"/>
        <source>boxplot|Box Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Covariance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="281"/>
        <source>cov|Covariance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="283"/>
        <source>Correlation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="287"/>
        <source>cor|Correlation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="292"/>
        <source>Scatter Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="296"/>
        <source>plot|Scatter Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="302"/>
        <source>Linear model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="306"/>
        <source>lm|Linear model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="310"/>
        <source>Anova</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="315"/>
        <source>BoxCox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="326"/>
        <source>Q-Q Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="332"/>
        <source>Q-Q Plot (residuals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="336"/>
        <source>$residuals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="341"/>
        <source>Shapiro-Wilk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="347"/>
        <source>Kolmogorov-Smirnov</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="352"/>
        <source>Durbin-Watson</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <location filename="mainwindow.cpp" line="363"/>
        <source>Square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="365"/>
        <location filename="mainwindow.cpp" line="369"/>
        <source>Exponentiation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="371"/>
        <location filename="mainwindow.cpp" line="375"/>
        <source>Square root</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="377"/>
        <location filename="mainwindow.cpp" line="381"/>
        <source>Natural logarithm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="383"/>
        <location filename="mainwindow.cpp" line="387"/>
        <source>Base 10 logarithm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="389"/>
        <location filename="mainwindow.cpp" line="393"/>
        <source>Reciprocal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="395"/>
        <location filename="mainwindow.cpp" line="399"/>
        <source>Reciprocal square root</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="405"/>
        <source>Time Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="410"/>
        <source>Differentiation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="416"/>
        <source>Autocorrelations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="422"/>
        <source>ARIMA (p,d,q)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="429"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="435"/>
        <source>&amp;R-squared help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="447"/>
        <source>Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="449"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="480"/>
        <source>&lt;b&gt;R-squared project&lt;/b&gt; is Free Software released under the GNU/GPLv3 License.&lt;br/&gt; &lt;br/&gt;&lt;strong&gt;Source code: &lt;/strong&gt;&lt;br/&gt;&lt;a href=&quot;https://gitlab.com/r-squared/r-squared&quot;&gt;https://gitlab.com/r-squared/r-squared&lt;/a&gt;&lt;br/&gt;&lt;strong&gt;Web page: &lt;/strong&gt;&lt;br/&gt;&lt;a href=&quot;http://rsquaredproject.wordpress.com/&quot;&gt;http://rsquaredproject.wordpress.com/&lt;/a&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;strong&gt;Authors:&lt;/strong&gt; &lt;br/&gt; Eduardo Nacimiento García &lt;br/&gt;Andrés Nacimiento García &lt;br/&gt;&lt;strong&gt;Colaborators:&lt;/strong&gt; &lt;br/&gt; Holi Díaz Kaas &lt;br/&gt;&lt;strong&gt;Version:&lt;/strong&gt; &lt;br/&gt; 0.2 alpha - Development</source>
        <oldsource>&lt;b&gt;R-squared project&lt;/b&gt; is Free Software released under the GNU/GPLv3 License.&lt;br/&gt; &lt;br/&gt;&lt;strong&gt;Source code: &lt;/strong&gt;&lt;br/&gt;&lt;a href=&quot;https://gitorious.org/r-squared&quot;&gt;https://gitorious.org/r-squared&lt;/a&gt;&lt;br/&gt;&lt;strong&gt;Web page: &lt;/strong&gt;&lt;br/&gt;&lt;a href=&quot;http://rsquaredproject.wordpress.com/&quot;&gt;http://rsquaredproject.wordpress.com/&lt;/a&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;strong&gt;Authors:&lt;/strong&gt; &lt;br/&gt; Eduardo Nacimiento García &lt;br/&gt;Andrés Nacimiento García &lt;br/&gt;&lt;strong&gt;Version:&lt;/strong&gt; &lt;br/&gt; 0.2 alpha - Development</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="680"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="764"/>
        <source>The document was modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="765"/>
        <source>Do you want to save changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="787"/>
        <source>Close workspace?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="datagrid.cpp" line="52"/>
        <source>SELECT ALL VARS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="54"/>
        <source>Select a variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="55"/>
        <source>Variable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="89"/>
        <source>Select variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="96"/>
        <source>X variable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="98"/>
        <source>Y variable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="126"/>
        <source>New variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="127"/>
        <source>Var name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="132"/>
        <source>Number of rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="133"/>
        <source>Number:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="190"/>
        <source>Rename varname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="191"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="219"/>
        <source>Edit cell content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="220"/>
        <source>Content of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="files.cpp" line="40"/>
        <source>Open file CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="files.cpp" line="42"/>
        <source>Files csv (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="files.cpp" line="92"/>
        <source>Open workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="files.cpp" line="94"/>
        <location filename="files.cpp" line="143"/>
        <source>Files RData (*.RData)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="files.cpp" line="141"/>
        <source>Save workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="205"/>
        <source>New model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="206"/>
        <location filename="statistics.cpp" line="682"/>
        <source>Model name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="293"/>
        <source>Exponent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="294"/>
        <source>n:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="408"/>
        <location filename="statistics.cpp" line="412"/>
        <location filename="statistics.cpp" line="416"/>
        <source>Select a model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="409"/>
        <source>Models:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="413"/>
        <source>ARIMA:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="417"/>
        <source>Time Series:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="608"/>
        <source>Time Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="615"/>
        <source>Time serie name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="619"/>
        <source>Start from: (e.g: 1983)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="623"/>
        <source>Frequency: (e.g: 12)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="657"/>
        <location filename="statistics.cpp" line="788"/>
        <source>All fields are needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="681"/>
        <source>New Time Serie Differentiation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="742"/>
        <source>ARIMA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="750"/>
        <source>ARIMA name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="754"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="758"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="762"/>
        <source>q</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>editor</name>
    <message>
        <location filename="editor.cpp" line="43"/>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="45"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="47"/>
        <source>&amp;Save as PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="50"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="53"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="56"/>
        <source>&amp;Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="59"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="62"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="66"/>
        <source>&amp;Insert table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="68"/>
        <source>&amp;Insert image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="70"/>
        <source>&amp;Fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="72"/>
        <source>&amp;Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="74"/>
        <source>&amp;Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="76"/>
        <source>&amp;Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="78"/>
        <source>&amp;Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="80"/>
        <source>&amp;Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="82"/>
        <source>&amp;Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="84"/>
        <source>&amp;Justify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="87"/>
        <source>Editor toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="225"/>
        <source>Save file html</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="227"/>
        <location filename="editor.cpp" line="264"/>
        <source>Files html (*.html)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="262"/>
        <source>Open file html</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="307"/>
        <location filename="editor.cpp" line="308"/>
        <source>Size of the table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="320"/>
        <source>Select an image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="editor.cpp" line="321"/>
        <source>PNG (*.png)
JPEG (*.jpg *jpeg)
GIF (*.gif)
Bitmap Files (*.bmp)
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
