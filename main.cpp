/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include <QPixmap>
#include <QSplashScreen>

int main(int argc, char *argv[])
{

 //   RInside R(argc, argv);
    QApplication a(argc, argv);


    QPixmap pixmap(":/images/images/logo.png");
    QSplashScreen splash(pixmap);
    splash.show();
    a.processEvents();


     QTranslator translator;
     translator.load("r-squared-project_" + QLocale::system().name()); //, "../r-squared/");
     a.installTranslator(&translator);

    MainWindow w;

    w.show();
    splash.finish(&w);

    return a.exec();
}
