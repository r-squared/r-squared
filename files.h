/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/


#ifndef FILES_H
#define FILES_H

#include <QWidget>
#include <QFile>
#include <QFileDialog>
#include <RInside.h>
#include <QTableWidget>
#include <QStringList>
#include <QString>
#include <QFileDialog>
#include <QMainWindow>

#include "mainwindow.h"

class MainWindow;

class Files {

public:

    Files(MainWindow *w); // Constructor
    ~Files(); // Destructor
    void openFile();
    void loadWorkspace();
    void saveWorkspace();
    void newProject();
    void loadFileConfiguration();
    void loadFileFunctionsR();

private:
    MainWindow *w_;
    void reloadSources();

};

#endif // FILES_H
