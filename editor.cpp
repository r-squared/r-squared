/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "editor.h"

editor::editor(QWidget *parent) :
    QWidget(parent)
{

    lytEditor_ = new QGridLayout(this);
    lytEditor_->setMargin(0);
    this->setLayout(lytEditor_);

    txtEditor_ = new QTextEdit(this);
    txtEditor_->setAcceptRichText(true);
    txtEditor_->setAcceptDrops(true);
    lytEditor_->addWidget(txtEditor_,1,0,1,1);

    createTableToolBar();

}

void editor::createTableToolBar() {

    actFileOpen_ = new QAction(QIcon(":/icons/icons/document-open.png"), tr("&Open"), this);
    actFileOpen_ -> setShortcut(QKeySequence(Qt::CTRL + Qt::Key_O));
    actFileSave_ = new QAction(QIcon(":/icons/icons/document-save.png"), tr("&Save"), this);
    actFileSave_ -> setShortcut(QKeySequence(Qt::CTRL + Qt::Key_S));
    actFilePdf_ = new QAction(QIcon(":/icons/icons/create-document-pdf.png"), tr("&Save as PDF"), this);
    actFilePdf_ -> setShortcut(QKeySequence(Qt::CTRL + Qt::Key_P));

    actEditCopy_ = new QAction(QIcon(":icons/icons/edit-copy.png"), tr("&Copy"), this);
    actEditCopy_->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_C));

    actEditPaste_ = new QAction(QIcon(":/icons/icons/edit-paste.png"), tr("&Paste"), this);
    actEditPaste_->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_V));

    actEditCut_ = new QAction(QIcon(":/icons/icons/edit-cut.png"), tr("&Cut"), this);
    actEditCut_->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_X));

    actEditUndo_ = new QAction(QIcon(":/icons/icons/edit-undo.png"), tr("&Undo"), this);
    actEditUndo_->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Z));

    actEditRedo_ = new QAction(QIcon(":/icons/icons/edit-redo.png"), tr("&Redo"), this);
    actEditRedo_->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_Z));


    actInsertTable_ = new QAction(QIcon(":/icons/icons/table.png"), tr("&Insert table"), this);

    actInsertImage_ = new QAction(QIcon(":/icons/icons/image.png"), tr("&Insert image"), this);

    actFormatFont_ = new QAction(QIcon(":/icons/icons/fonts.png"), tr("&Fonts"), this);

    actFormatBold_ = new QAction(QIcon(":/icons/icons/format-bold.png"), tr("&Bold"), this);

    actFormatItalic_ = new QAction(QIcon(":/icons/icons/format-italic.png"), tr("&Italic"), this);

    actFormatUnderline_ = new QAction(QIcon(":/icons/icons/format-underline.png"), tr("&Underline"), this);

    actFormatCenter_ = new QAction(QIcon(":/icons/icons/format-center.png"), tr("&Center"), this);

    actFormatLeft_ = new QAction(QIcon(":/icons/icons/format-left.png"), tr("&Left"), this);

    actFormatRight_ = new QAction(QIcon(":/icons/icons/format-right.png"), tr("&Right"), this);

    actFormatJustify_ = new QAction(QIcon(":/icons/icons/format-justify.png"), tr("&Justify"), this);


    editorToolBar_ = new QToolBar(tr("Editor toolbar"));

    editorToolBar_->addAction(actFileOpen_);
    editorToolBar_->addAction(actFileSave_);
    editorToolBar_->addAction(actFilePdf_);


    editorToolBar_ ->addSeparator();
    editorToolBar_->addAction(actEditCut_);
    editorToolBar_->addAction(actEditCopy_);
    editorToolBar_->addAction(actEditPaste_);
    editorToolBar_->addAction(actEditUndo_);
    editorToolBar_->addAction(actEditRedo_);

    editorToolBar_->addSeparator();
    editorToolBar_->addAction(actInsertImage_);
    editorToolBar_->addAction(actInsertTable_);

    editorToolBar_ ->addSeparator();
    editorToolBar_ -> addAction(actFormatBold_);
    editorToolBar_ -> addAction(actFormatItalic_);
    editorToolBar_ -> addAction(actFormatUnderline_);
    editorToolBar_ -> addAction(actFormatCenter_);
    editorToolBar_ -> addAction(actFormatLeft_);
    editorToolBar_ -> addAction(actFormatRight_);
    editorToolBar_ -> addAction(actFormatJustify_);

    editorToolBar_ -> addAction(actFormatFont_);


    actFormatFontCombo_ = new QFontComboBox(this);
    actFormatFontCombo_->setMaximumWidth(160);
    actFormatFontCombo_ ->setEditable(true);

    actFormatSize_ = new QComboBox;
    actFormatSize_ ->setEditable(true);
    QStringList list = (QStringList()<<"10"<<"12"<<"14"<<"16"<<"22"<<"36"<<"52");
    actFormatSize_->addItems(list);

    editorToolBar_-> addWidget(actFormatSize_);
    editorToolBar_-> addWidget(actFormatFontCombo_);


    connect(actFileOpen_, SIGNAL(triggered()), this, SLOT(openDocument()));
    connect(actFileSave_, SIGNAL(triggered()), this, SLOT(saveDocument()));
    connect(actFilePdf_, SIGNAL(triggered()), this, SLOT(saveDocumentPdf()));

    connect(actEditCopy_, SIGNAL(triggered()), txtEditor_, SLOT(copy()));
    connect(actEditCut_, SIGNAL(triggered()), txtEditor_, SLOT(cut()));
    connect(actEditPaste_, SIGNAL(triggered()), txtEditor_, SLOT(paste()));
    connect(actEditUndo_, SIGNAL(triggered()), txtEditor_, SLOT(undo()));
    connect(actEditRedo_, SIGNAL(triggered()), txtEditor_, SLOT(redo()));

    connect(actInsertImage_, SIGNAL(triggered()), this, SLOT(insertImage()));
    connect(actInsertTable_, SIGNAL(triggered()), this, SLOT(insertTable()));


    connect(actFormatBold_, SIGNAL(triggered()), this, SLOT(setTextBold()));
    connect(actFormatItalic_, SIGNAL(triggered()), this, SLOT(setTextItalic()));
    connect(actFormatUnderline_, SIGNAL(triggered()), this, SLOT(setTextUnderline()));
    connect(actFormatCenter_, SIGNAL(triggered()), this, SLOT(setTextCenter()));
    connect(actFormatLeft_, SIGNAL(triggered()), this, SLOT(setTextLeft()));
    connect(actFormatRight_, SIGNAL(triggered()), this, SLOT(setTextRight()));
    connect(actFormatJustify_, SIGNAL(triggered()), this, SLOT(setTextJustify()));
    connect(actFormatFont_, SIGNAL(triggered()), this, SLOT(changeFont()));

    connect(actFormatSize_, SIGNAL(editTextChanged ( const QString &)), this, SLOT(setTextSize(const QString &)));
    connect(actFormatFontCombo_, SIGNAL(currentFontChanged(const QFont &)), this, SLOT(setFontCombo(const QFont &)));

    lytEditor_->addWidget(editorToolBar_,0,0,1,1);
}



void editor::setTextBold() {
    QTextCursor     cursor = txtEditor_->textCursor();
    QTextCharFormat formato = cursor.charFormat();
    QFont font;
    if (formato.fontWeight() == QFont::Bold) {
        font.setBold(false);
    }
    else {
        font.setBold(true);
    }
    formato.setFont(font);
    cursor.setCharFormat(formato);
}


void editor::setTextItalic() {
    QTextCursor     cursor = txtEditor_->textCursor();
    QTextCharFormat formato = cursor.charFormat();
    QFont font;
    if (formato.fontItalic()) {
        font.setItalic(false);
    }
    else {
        font.setItalic(true);
    }
    formato.setFont(font);
    cursor.setCharFormat(formato);
}


void editor::setTextUnderline() {
    QTextCursor     cursor = txtEditor_->textCursor();
    QTextCharFormat formato = cursor.charFormat();
    QFont font;
    if (formato.underlineStyle()) {
        font.setUnderline(false);
    }
    else {
        font.setUnderline(true);
    }
    formato.setFont(font);
    cursor.setCharFormat(formato);
}

void editor::setTextCenter() {
    txtEditor_->setAlignment(Qt::AlignCenter);
}

void editor::setTextLeft() {
    txtEditor_->setAlignment(Qt::AlignLeft);
}

void editor::setTextRight() {
    txtEditor_->setAlignment(Qt::AlignRight);
}

void editor::setTextJustify() {
    txtEditor_->setAlignment(Qt::AlignJustify);
}


void editor::saveDocument() {
    QString fileName;
    fileName = QFileDialog::getSaveFileName(this,
                                                 tr("Save file html"),
                                                 "",
                                                 tr("Files html (*.html)"));
    if (fileName != "") {
            QFile file;
            if (QFileInfo(fileName).suffix().isEmpty()) {
                        fileName.append(".html");
            }
            if (file.open(QFile::WriteOnly | QFile::Truncate)) {
                file.write(txtEditor_->toHtml().toUtf8());
                file.close();
            }
            QTextDocument * textDocument = txtEditor_->document();
            textDocument->setModified(false);
        }

}


void editor::saveDocumentPdf() {
    QString fileName = QFileDialog::getSaveFileName(this, "Export PDF",
                                                       QString(), "*.pdf");
    if (!fileName.isEmpty()) {
       if (QFileInfo(fileName).suffix().isEmpty())
           fileName.append(".pdf");
       QPrinter printer(QPrinter::HighResolution);
       printer.setOutputFormat(QPrinter::PdfFormat);
       printer.setPageSize(QPrinter::A4);
       printer.setOutputFileName(fileName);
       txtEditor_->document()->print(&printer);
    }
}


void editor::openDocument() {
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this,
                                                 tr("Open file html"),
                                                 "",
                                                 tr("Files html (*.html)"));
    if (fileName != "") {
        QFile file;
        file.setFileName(fileName);
        if (file.open(QFile::ReadOnly)) {
            txtEditor_ -> setText(file.readAll());
            file.close();
        }
    }
}


void editor::setFontCombo(const QFont &font) {
    QTextCursor     cursor = txtEditor_->textCursor();
    QTextCharFormat formato = cursor.charFormat();
    formato.setFont(font);
    cursor.setCharFormat(formato);
}

void editor::setTextSize(const QString &size_s) {
    QTextCursor     cursor = txtEditor_->textCursor();
    QTextCharFormat format = cursor.charFormat();
    QFont font;
    int size = size_s.toInt();
    font.setPointSize(size);
    format.setFont(font);
    cursor.setCharFormat(format);
}


void editor::changeFont() {
    bool ok;
    QFont font = QFontDialog::getFont(&ok, txtEditor_->font(), this);
    if (ok) {
        QTextCursor     cursor = txtEditor_->textCursor();
        QTextCharFormat format = cursor.charFormat();
        format.setFont(font);
        cursor.setCharFormat(format);
    }
}


void editor::insertTable() {
    int rows = QInputDialog::getInt(this, tr("Size of the table"), "Rows:", 1);
    int cols = QInputDialog::getInt(this, tr("Size of the table"), "Columns:", 1);

    QTextTableFormat tableFormat;
    tableFormat.setBorderStyle(QTextFrameFormat::BorderStyle_Solid);
    tableFormat.setCellPadding(1);
    tableFormat.setWidth(QTextLength(QTextLength::PercentageLength, 100));
    tableFormat.setAlignment(Qt::AlignLeft);
    QTextCursor cursor = txtEditor_->textCursor();
    cursor.insertTable(rows, cols, tableFormat);
}

void editor::insertImage() {
    QString file = QFileDialog::getOpenFileName(this, tr("Select an image"),
                                      ".", tr("PNG (*.png)\n"
                                        "JPEG (*.jpg *jpeg)\n"
                                        "GIF (*.gif)\n"
                                        "Bitmap Files (*.bmp)\n"));
    QUrl Uri ( QString ( "file://%1" ).arg ( file ) );
    QImage image = QImageReader ( file ).read();

    QTextDocument * textDocument = txtEditor_->document();
    textDocument->addResource( QTextDocument::ImageResource, Uri, QVariant ( image ) );
    QTextCursor cursor = txtEditor_->textCursor();
    QTextImageFormat imageFormat;
    int x, y;
    if (720 < image.width()) {
        imageFormat.setWidth(720);
        x = (100 * 720) / image.width();
        y = (image.height() * x) / 100;
        imageFormat.setHeight( y );
    }
    else {
        imageFormat.setWidth( image.width() );
        imageFormat.setHeight( image.height() );
    }
    imageFormat.setName( Uri.toString() );
    cursor.insertImage(imageFormat);
}


QTextEdit* editor::getTxtEditor() {
    return txtEditor_;
}
