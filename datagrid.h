#ifndef DATAGRID_H
#define DATAGRID_H

#include "mainwindow.h"
#include "generic.h"
#include <QPushButton>
#include <QCheckBox>
#include <QVBoxLayout>
#include <QTabWidget>
#include <QGroupBox>
#include <QFileDialog>

class MainWindow;

class DataGrid {

public:

    DataGrid(MainWindow *w); // Constructor
    ~DataGrid(); // Destructor
    int CheckColumn(int type);
    QList<int> CheckColumn2();
    QList<int> CheckColumnGeneric(int size, QVector<RArguments> opRA);
    void createConfiguration();
    void newVariable ();
    void renameVar();
    void editCell();
    void editCell(int row, int col);
    void closeConfTab();
    void saveConfigurationTab();

private:
    MainWindow *w_;



};

#endif // DATAGRID_H
