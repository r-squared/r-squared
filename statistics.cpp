/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "statistics.h"

// Constructor
Statistics::Statistics(MainWindow *w) {
    w_ = w;
    dg_ = new DataGrid(w);
    allModels(false, "lm");
    allModels(false, "ts");
    allModels(false, "arima");
}

// Destructor
Statistics::~Statistics() {}


// GENERIC. Operations with generic functions
void Statistics::genericFunction(QString operation, QMap<QString, RFunctions>* rf) {

    RFunctions opRF = rf->value(operation);
    QString op = opRF.name + "|" + opRF.title;

    QVector<RArguments> opRA = opRF.arguments;

    int objCount = 0;
    foreach (RArguments arg, opRA) {
        if (arg.type.toStdString() == "object") {
            objCount++;
        }
    }

    QList<int> col_num_list = dg_->CheckColumnGeneric(objCount, opRA);
    if (col_num_list.first() == -1) {
        return;
    }

    QString mydata;

    w_->resetFile();

    if (opRF.output == "text") {
        mydata = "(";
        foreach (int item, col_num_list) {
            QString col;
            col.append(QString("%1").arg(item + 1));
            mydata += "mydata[,"+col+"],";
        }
        mydata.insert(mydata.length()-1, ")");
        w_->R_.parseEval("HTML(z <- "+opRF.name.toStdString()+mydata.toStdString()+", file=\""+QDir::tempPath().toStdString()+"/out.html\")");
        Rcpp::CharacterVector z = w_->R_.parseEval("z");
        std::string zz = Rcpp::as<std::string>(z) + "\n";

        w_->logTab_->appendPlainText(opRF.name + mydata);
        w_->logTab_->appendPlainText(QString::fromLocal8Bit(zz.c_str()));

        w_->R_.parseEvalQ("rm(z)");
    }
    else if (opRF.output == "graphs") {

        SEXP ans;
        ans = w_->R_.parseEval("names(mydata)");
        Rcpp::CharacterVector names(ans);


        mydata = "(";
        foreach (int item, col_num_list) {
            QString col;
            col.append(QString("%1").arg(item + 1));
            mydata += "mydata[,"+col+"],";
        }

        mydata += " main=\"";
        QVector<QString> label;
        label.push_back("xlab");
        label.push_back("ylab");
        label.push_back("zlab");

        QString main = "";
        QString labels = "";
        int i = 0;
        foreach (int item, col_num_list) {
            QString col;
            col.append(QString("%1").arg(item + 1));
            QString lab = label.at(i);
            main += QString(names[col.toInt()-1])+",";
            labels += lab+"=\""+QString(names[col.toInt()-1])+"\",";
            i++;
        }

        main.replace(main.length()-1, 1, "\"");
        labels.replace(labels.length()-1, 1, ")");

        mydata += main + ", ";
        mydata += labels;

        QString img_idx = QString::number(std::time(0));
        QString eval = "png(\""+QDir::tempPath()+"/"+opRF.title+img_idx+".png\"); ";
        eval += opRF.name+mydata+"; dev.off()";

        w_->resetFile();
        w_->R_.parseEvalQ(eval.toStdString());

        eval = "HTMLInsertGraph(\""+QDir::tempPath()+"/"+opRF.title+img_idx+".png\" ,file=\""+QDir::tempPath()+"/out.html\")";
        w_->R_.parseEvalQ(eval.toStdString());


        w_->logTab_->appendPlainText(opRF.name + mydata + "\n");
    }

    w_->addHtml(opRF.title);
    w_->tabs_->setCurrentWidget(w_->txtWidget_);

}


// SUMARY. Create a summary in Editor tab
void Statistics::summary(){

    int col_num = dg_->CheckColumn(0);
    if (col_num == -1) {
        return;
    }

    QString col;
    col.append(QString("%1").arg(col_num + 1));

    w_->resetFile();
    if (col_num == -2) {
        w_->R_.parseEval("HTML(z <- summary(mydata), file=\""+QDir::tempPath().toStdString()+"/out.html\")");
    }
    else {
        w_->R_.parseEval("HTML(z <- summary(mydata["+col.toStdString()+"]), file=\""+QDir::tempPath().toStdString()+"/out.html\")");
    }
    w_->R_.parseEvalQ("rm(z)");
    w_->addHtml("Descriptive statistics");

    w_->tabs_->setCurrentWidget(w_->txtWidget_);

}


// UNIDIMENSIONAL. Operations with unidimensional variables
void Statistics::unidimensional(QString operation) {

    int col_num = dg_->CheckColumn(0);
    if (col_num == -1) {
        return;
    }

    QStringList operationList = operation.split("|");

    QString col;
    col.append(QString("%1").arg(col_num + 1));

    w_->resetFile();
    w_->R_.parseEval("HTML(z <- "+operationList[0].toStdString()+"(mydata[,"+col.toStdString()+"]), file=\""+QDir::tempPath().toStdString()+"/out.html\")");
    w_->R_.parseEvalQ("rm(z)");
    w_->addHtml(operationList[1]);
    w_->tabs_->setCurrentWidget(w_->txtWidget_);

}

// UNIDIMENSIONAL GRAPH. Operations' graph with unidimensional variables
void Statistics::unidimensionalGraph(QString operation) {

    int col_num = dg_->CheckColumn(0);
    if (col_num == -1) {
        return;
    }

    QStringList operationList = operation.split("|");
    QString col;
    col.append(QString("%1").arg(col_num + 1));

    SEXP ans;
    ans = w_->R_.parseEval("names(mydata)");
    Rcpp::CharacterVector names(ans);


    QString img_idx = QString::number(std::time(0));
    QString eval = "png(\""+QDir::tempPath()+"/"+operationList[1]+img_idx+".png\"); ";
    eval += operationList[0]+"(mydata[,"+col+"], main=\""+names[col.toInt() - 1]+"\", xlab=\""+names[col.toInt() - 1]+"\"); dev.off()";

    w_->resetFile();
    w_->R_.parseEvalQ(eval.toStdString());

    eval = "HTMLInsertGraph(\""+QDir::tempPath()+"/"+operationList[1]+img_idx+".png\" ,file=\""+QDir::tempPath()+"/out.html\")";
    w_->R_.parseEvalQ(eval.toStdString());
    w_->addHtml(operationList[1]);
    w_->tabs_->setCurrentWidget(w_->txtWidget_);

}

// BIDIMENSIONAL. Operations with bidimensional variables
void Statistics::bidimensional(QString operation) {

    QList<int> col_num_list = dg_->CheckColumn2();
    int x = col_num_list.first();
    if (x == -1) {
        return;
    }
    int y = col_num_list.last();

    QStringList operationList = operation.split("|");
    QString col_x;
    QString col_y;
    col_x.append(QString("%1").arg(x + 1));
    col_y.append(QString("%1").arg(y + 1));

    w_->resetFile();
    w_->R_.parseEval("HTML(z <- "+operationList[0].toStdString()+"(mydata[,"+col_x.toStdString()+"],mydata[,"+col_y.toStdString()+"]), file=\""+QDir::tempPath().toStdString()+"/out.html\")");
    w_->R_.parseEvalQ("rm(z)");
    w_->addHtml(operationList[1]);
    w_->tabs_->setCurrentWidget(w_->txtWidget_);

}

// BIDIMENSIONAL GRAPH. Operations' graph with bidimensional variables
void Statistics::bidimensionalGraph(QString operation) {

    QList<int> col_num_list = dg_->CheckColumn2();
    int x = col_num_list.first();
    if (x == -1) {
        return;
    }
    int y = col_num_list.last();

    QStringList operationList = operation.split("|");
    QString col_x;
    QString col_y;
    col_x.append(QString("%1").arg(x + 1));
    col_y.append(QString("%1").arg(y + 1));

    SEXP ans;
    ans = w_->R_.parseEval("names(mydata)");
    Rcpp::CharacterVector names(ans);


    QString img_idx = QString::number(std::time(0));
    QString eval = "png(\""+QDir::tempPath()+"/"+operationList[1]+img_idx+".png\"); ";
    eval += operationList[0]+"(mydata[,"+ col_x+"],mydata[,"+col_y+"], main=\""+names[col_x.toInt() - 1]+", "+names[col_y.toInt() - 1] +
            "\", xlab=\""+names[col_x.toInt() - 1]+"\", ylab=\""+names[col_y.toInt() - 1]+"\"); dev.off()";

    w_->resetFile();
    w_->R_.parseEvalQ(eval.toStdString());

    eval = "HTMLInsertGraph(\""+QDir::tempPath()+"/"+operationList[1]+img_idx+".png\" ,file=\""+QDir::tempPath()+"/out.html\")";
    w_->R_.parseEvalQ(eval.toStdString());
    w_->addHtml(operationList[1]);
    w_->tabs_->setCurrentWidget(w_->txtWidget_);

}

// FITTING MODELS. Generate a new lineal model
void Statistics::fittingModels(QString operation) {

    QList<int> col_num_list = dg_->CheckColumn2();
    int x = col_num_list.first();
    if (x == -1) {
        return;
    }
    bool ok;
    QString text = QInputDialog::getText(w_, QObject::tr("New model"),
                                         QObject::tr("Model name:"), QLineEdit::Normal,
                                         "Mod"+QString::number(w_->listModels_.length() + 1), &ok);
    if (ok && !text.isEmpty()) {

        int y = col_num_list.last();

        QStringList operationList = operation.split("|");
        QString col_x;
        QString col_y;
        col_x.append(QString("%1").arg(x + 1));
        col_y.append(QString("%1").arg(y + 1));

        w_->R_.parseEval(text.toStdString()+" <- "+operationList[0].toStdString()+"(mydata[,"+col_y.toStdString()+"]~mydata[,"+col_x.toStdString()+"])");

        w_->resetFile();
        w_->R_.parseEvalQ("HTML(summary("+text.toStdString()+"), file=\""+QDir::tempPath().toStdString()+"/out.html\")");
        w_->addHtml(operationList[1]);

        allModels(false, "lm");
        w_->tabs_->setCurrentWidget(w_->txtWidget_);
    }
    else {
        return;
    }
}



// Analysis of variance
void Statistics::anova() {


    QString text = allModels(true, "lm");

    if (text == "-1") {
        return;
    }
    w_->resetFile();
    w_->R_.parseEvalQ("HTML(anova("+text.toStdString()+"), file=\""+QDir::tempPath().toStdString()+"/out.html\")");
    w_->addHtml("<br><br>ANOVA");

    QString img_idx = QString::number(std::time(0));
    QString eval = "png(\""+QDir::tempPath()+"/anova"+img_idx+".png\"); par(mfrow=c(2,2));";
    eval += "plot("+text+"); dev.off()";




    w_->resetFile();
    w_->R_.parseEvalQ(eval.toStdString());
    eval = "HTMLInsertGraph(\""+QDir::tempPath()+"/anova"+img_idx+".png\" ,file=\""+QDir::tempPath()+"/out.html\")";
    w_->R_.parseEvalQ(eval.toStdString());
    w_->addHtml("ANOVA");
    w_->tabs_->setCurrentWidget(w_->txtWidget_);

}


// Transformed
void Statistics::transformed(QString operation) {
    int col_num = dg_->CheckColumn(1);
    if (col_num == -1) {
        return;
    }

    QString trans;

    if (operation == "Square") {
        trans = "mydata[,"+QString::number(col_num + 1)+"]^2";
    }
    else if (operation == "Square root") {
        trans = "sqrt(mydata[,"+QString::number(col_num + 1)+"])";
    }
    else if (operation == "Natural logarithm") {
        trans = "log(mydata[,"+QString::number(col_num + 1)+"])";
    }
    else if (operation == "Base 10 logarithm") {
        trans = "log10(mydata[,"+QString::number(col_num + 1)+"])";
    }
    else if (operation == "Reciprocal") {
        trans = "(1/mydata[,"+QString::number(col_num + 1)+"])";
    }
    else if (operation == "Reciprocal square root") {
        trans = "(1/sqrt(mydata[,"+QString::number(col_num + 1)+"]))";
    }
    else if (operation == "Exponentiation") {
        bool ok;
        QString text = QInputDialog::getText(w_, QObject::tr("Exponent"),
                                             QObject::tr("n:"), QLineEdit::Normal,
                                             QString::number(2), &ok);
        if (ok && !text.isEmpty()) {
            text.replace(",", ".");
            trans = "mydata[,"+QString::number(col_num + 1)+"]^"+text;
        }
        else {
            return;
        }
    }
    else {
        return;
    }

    SEXP ans;
    w_->totalCols_++;


    ans = w_->R_.parseEval("mydata[,"+QString::number(w_->totalCols_).toStdString()+"] <- with(mydata, "+trans.toStdString()+")");

    Rcpp::DataFrame DF(ans);

    QVector<Rcpp::StringVector> dataVector;

    ans = w_->R_.parseEval("names(mydata)");
    Rcpp::CharacterVector names(ans);


    Rcpp::StringVector tmpDF = DF[0];
    dataVector.append(tmpDF);
    w_->listHeader_ << (QString)names[w_->totalCols_ - 1];

    for (int row = 0; row < dataVector[0].size(); row++) {
        QString data = (QString)dataVector[0][row];
        data.replace(",", ".");
        w_->gridMain_->setItem(row, w_->totalCols_ -1, new QTableWidgetItem (data));
    }
    w_->gridMain_->setHorizontalHeaderLabels(w_->listHeader_);
}


// Show all objects of the type
// Types:
//   "lm" : Linear Models
//   "arima" : Autoregressive integrated moving average models
//   "ts" : Time Series
QString Statistics::allModels(bool dialog, QString type) {


    QString eval = "result <- vector(); "
    "cont <- 1; "
    "for (i in 1:(length(ls())+1)) { "
    "  if (!is.data.frame(eval(parse(text=ls()[i])))) { "
    "    if (ls()[i] != \"cont\" && ls()[i] != \"result\" && ls()[i] != \"i\") { ";

    if (type == "lm") {
        eval += "       if (!is.ts(eval(parse(text=ls()[i])))) { ";
                "           if (length(grep(\"^lm\",eval(parse(text=ls()[i]))$call))) { ";
    }
    else if (type == "arima") {
        eval += "       if (!is.ts(eval(parse(text=ls()[i])))) { ";
                "           if (length(grep(\"^arima\",eval(parse(text=ls()[i]))$call))) {";
    }
    else if (type == "ts") {
        eval += "       if (is.ts(eval(parse(text=ls()[i])))) {";
    }
    else {
        return "-1";
    }

    eval += "        result[cont]<- ls()[i]; "
            "        cont <- cont + 1 }}}}";

    /*if (type != "ts") {
        eval += " } ";
    }*/


    w_->R_.parseEvalQ(eval.toStdString());

    Rcpp::StringVector modelsNames = w_->R_.parseEval("result");

    int size = 0;

    if (type == "lm") {
        w_->listModels_.clear();
        for (int i = 0; i < modelsNames.length(); i++) {
            w_->listModels_ << (QString)modelsNames[i];
        }
        size = w_->listModels_.length();
    }
    else if (type == "arima") {
        w_->listARIMA_.clear();
        for (int i = 0; i < modelsNames.length(); i++) {
            w_->listARIMA_ << (QString)modelsNames[i];
        }
        size = w_->listARIMA_.length();
    }
    else if (type == "ts") {
        w_->listTimeSeries_.clear();
        for (int i = 0; i < modelsNames.length(); i++) {
            w_->listTimeSeries_ << (QString)modelsNames[i];
        }
        size = w_->listTimeSeries_.length();
    }

    w_->R_.parseEvalQ("rm(cont); rm(i); rm(result)");

    if (dialog) {

        bool ok;
        if (size > 0) {
            QString text = "";
            if (type == "lm") {
                text = QInputDialog::getItem(w_, QObject::tr("Select a model"),
                                                     QObject::tr("Models:"), w_->listModels_, 0, false, &ok);
            }
            else if (type == "arima") {
                text = QInputDialog::getItem(w_, QObject::tr("Select a model"),
                                                     QObject::tr("ARIMA:"), w_->listARIMA_, 0, false, &ok);
            }
            else if (type == "ts") {
                text = QInputDialog::getItem(w_, QObject::tr("Select a model"),
                                                     QObject::tr("Time Series:"), w_->listTimeSeries_, 0, false, &ok);
            }

            if (ok && !text.isEmpty()) {
                return text;
            }
            else {
                return "-1";
            }
        }
        else {
            return "-1";
        }
    }

    return "-2";
}

// BoxCox Graph
void Statistics::boxCox() {

    QString model = allModels(true, "lm");

    if (model == "-1") {
        return;
    }

    w_->R_.parseEvalQ("library(MASS)");
    QString img_idx = QString::number(std::time(0));
    QString eval = "png(\""+QDir::tempPath()+"/boxcox"+model+img_idx+".png\"); ";
    eval += "boxcox("+model+"); dev.off()";

    w_->resetFile();
    w_->R_.parseEvalQ(eval.toStdString());

    eval = "HTMLInsertGraph(\""+QDir::tempPath()+"/boxcox"+model+img_idx+".png\" ,file=\""+QDir::tempPath()+"/out.html\")";
    w_->R_.parseEvalQ(eval.toStdString());
    w_->addHtml("BoxCox: "+model);
    w_->tabs_->setCurrentWidget(w_->txtWidget_);
}


// QQ Plot
void Statistics::qqPlot(QString operation) {

    QString img_idx = QString::number(std::time(0));
    QString eval = "png(\""+QDir::tempPath()+"/qqplot"+img_idx+".png\"); ";


    QString model;
    if (operation == "$residuals") {
        model = allModels(true, "lm");

        if (model == "-1") {
            return;
        }

        eval += "qqnorm("+model+operation+"); qqline("+model+operation+"); dev.off()";
    }

    else {
        int col_num = dg_->CheckColumn(1);
        if (col_num == -1) {
            return;
        }

        QString col;
        col.append(QString("%1").arg(col_num + 1));

        eval += "qqnorm(mydata[,"+col+"]); qqline(mydata[,"+col+"]); dev.off()";
    }




    w_->resetFile();
    w_->R_.parseEvalQ(eval.toStdString());

    eval = "HTMLInsertGraph(\""+QDir::tempPath()+"/qqplot"+img_idx+".png\" ,file=\""+QDir::tempPath()+"/out.html\")";
    w_->R_.parseEvalQ(eval.toStdString());
    if (operation == "$residuals") {
        w_->addHtml("Q-Q Plot (residuals): "+model);
    }
    else {
         w_->addHtml("Q-Q Plot");
    }
    w_->tabs_->setCurrentWidget(w_->txtWidget_);
}


// Histogram + curve (residuals)
void Statistics::curveHist() {

    QString model = allModels(true, "lm");

    if (model == "-1") {
        return;
    }

    w_->R_.parseEvalQ("mean_res=mean("+model.toStdString()+"$residuals)");
    w_->R_.parseEvalQ("sd_res=sd("+model.toStdString()+"$residuals)");


    QString img_idx = QString::number(std::time(0));
    QString eval = "png(\""+QDir::tempPath()+"/curveHist"+model+img_idx+".png\"); ";
    eval += "curve(dnorm("+model+"$residuals, mean=mean_res, sd=sd_res), 0, 0.0000001, col=\"red\", add=TRUE, lty=2); dev.off()";

    w_->resetFile();
    w_->R_.parseEvalQ(eval.toStdString());

    eval = "HTMLInsertGraph(\""+QDir::tempPath()+"/curveHist"+model+img_idx+".png\" ,file=\""+QDir::tempPath()+"/out.html\")";
    w_->R_.parseEvalQ(eval.toStdString());
    w_->addHtml("Histogram of residuals with nomral curve: "+model);
    w_->tabs_->setCurrentWidget(w_->txtWidget_);

    w_->R_.parseEvalQ("rm(mean_res)");
    w_->R_.parseEvalQ("rm(sd_res)");
}


// Test: Shapiro-Wilk
void Statistics::shapiroWilk() {

    QString model = allModels(true, "lm");

    if (model == "-1") {
        return;
    }

    w_->resetFile();
    w_->R_.parseEvalQ("HTML(z <- shapiro.test("+model.toStdString()+"$residuals), file=\""+QDir::tempPath().toStdString()+"/out.html\")");
    w_->R_.parseEvalQ("rm(z)");
    w_->addHtml("");

    w_->tabs_->setCurrentWidget(w_->txtWidget_);
}



// Test: Kolmogorov-Smirnov
void Statistics::kolmogorovSmirnov() {

    QString model = allModels(true, "lm");

    if (model == "-1") {
        return;w_->R_.parseEvalQ("rm(mean_res)");
        w_->R_.parseEvalQ("rm(sd_res)");
    }

    w_->resetFile();
    w_->R_.parseEvalQ("mean_res<-mean("+model.toStdString()+"$residuals)");
    w_->R_.parseEvalQ("sd_res<-mean("+model.toStdString()+"$residuals)");
    w_->R_.parseEvalQ("HTML(z <- ks.test("+model.toStdString()+"$residuals, \"pnorm\", mean=mean_res, sd=sd_res), file=\""+QDir::tempPath().toStdString()+"/out.html\")");
    w_->R_.parseEvalQ("rm(z)");
    w_->R_.parseEvalQ("rm(mean_res)");
    w_->R_.parseEvalQ("rm(sd_res)");
    w_->addHtml("");

    w_->tabs_->setCurrentWidget(w_->txtWidget_);
}



// Test: Durbin-Watson
void Statistics::durbinWatson() {

    QString model = allModels(true, "lm");

    if (model == "-1") {
        return;
    }

    w_->resetFile();
    w_->R_.parseEvalQ("library(lmtest)");
    w_->R_.parseEvalQ("HTML(z <- dwtest("+model.toStdString()+", alternative=\"two.sided\"), file=\""+QDir::tempPath().toStdString()+"/out.html\")");
    w_->R_.parseEvalQ("rm(z)");
    w_->addHtml("");

    w_->tabs_->setCurrentWidget(w_->txtWidget_);
}


// Time Series
void Statistics::timeSeries() {

    int col = dg_->CheckColumn(1);
    if (col == -1) {
        return;
    }

    QDialog dialog(w_);
    dialog.setWindowTitle(QObject::tr("Time Series"));
    QFormLayout form(&dialog);

    QLineEdit* name = new QLineEdit;
    QLineEdit* start = new QLineEdit;
    QLineEdit* freq = new QLineEdit;

    form.addRow(new QLabel(QObject::tr("Time serie name:")));
    name->setText("TS"+QString::number(w_->listTimeSeries_.length() + 1));
    form.addWidget(name);

    form.addRow(new QLabel(QObject::tr("Start from: (e.g: 1983)")));
    start->setText(QString::number(1));
    form.addWidget(start);

    form.addRow(new QLabel(QObject::tr("Frequency: (e.g: 12)")));
    freq->setText(QString::number(12));
    form.addWidget(freq);


    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                               Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    QObject::connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));


    if (dialog.exec() == QDialog::Accepted) {
        if (!name->text().isEmpty() && !start->text().isEmpty() && !freq->text().isEmpty()) {
            QString img_idx = QString::number(std::time(0));
            QString eval = name->text()+" <- ts(mydata[,"+QString::number(col+1)+"], start="+start->text()+", frequency="+freq->text()+")";
            w_->R_.parseEvalQ(eval.toStdString());

            eval = "png(\""+QDir::tempPath()+"/ts"+img_idx+".png\"); ";
            eval += "plot("+name->text()+"); dev.off()";

            w_->resetFile();
            w_->R_.parseEvalQ(eval.toStdString());

            eval = "HTMLInsertGraph(\""+QDir::tempPath()+"/ts"+img_idx+".png\" ,file=\""+QDir::tempPath()+"/out.html\")";
            w_->R_.parseEvalQ(eval.toStdString());
            w_->addHtml("Time Series");


            allModels(false, "ts");
            w_->tabs_->setCurrentWidget(w_->txtWidget_);
        }
        else {
            QMessageBox* msgBox = new QMessageBox(w_);
            msgBox->setText(QObject::tr("All fields are needed"));
            msgBox->setStandardButtons(QMessageBox::Ok);
            msgBox->setDefaultButton(QMessageBox::Ok);
            msgBox->exec();
            return;
        }
    }
    else {
        return;
    }

}

// Time Series Diff
void Statistics::timeSeriesDiff() {

    QString model = allModels(true, "ts");

    if (model == "-1") {
        return;
    }


    bool ok;
    QString text = QInputDialog::getText(w_, QObject::tr("New Time Serie Differentiation"),
                                         QObject::tr("Model name:"), QLineEdit::Normal,
                                         model+"Diff"+QString::number(w_->listTimeSeries_.length() + 1), &ok);
    if (ok && !text.isEmpty()) {
        QString img_idx = QString::number(std::time(0));
        QString eval = text+" <- diff("+model+")";
        w_->R_.parseEvalQ(eval.toStdString());

        eval = "png(\""+QDir::tempPath()+"/tsDiff"+img_idx+".png\"); ";
        eval += "plot("+text+"); dev.off()";

        w_->resetFile();
        w_->R_.parseEvalQ(eval.toStdString());

        eval = "HTMLInsertGraph(\""+QDir::tempPath()+"/tsDiff"+img_idx+".png\" ,file=\""+QDir::tempPath()+"/out.html\")";
        w_->R_.parseEvalQ(eval.toStdString());
        w_->addHtml("Time Series Differentiation");


        allModels(false, "ts");
        w_->tabs_->setCurrentWidget(w_->txtWidget_);
    }
}



// Autocorrelation functions ACF and PACF
void Statistics::autocorrelations() {

    QString model = allModels(true, "ts");

    if (model == "-1") {
        return;
    }

    QString img_idx = QString::number(std::time(0));
    QString eval = " png(\""+QDir::tempPath()+"/autocorrelation"+model+img_idx+".png\");"
                   " par(mfrow=c(2,1));"
                   " acf ("+model+", main=\"Autocorrelation Function "+model+"\");"
                   " pacf ("+model+", main=\"Partial Autocorrelation Function "+model+"\");"
                   " dev.off()";
    w_->resetFile();
    w_->R_.parseEvalQ(eval.toStdString());

    eval = "HTMLInsertGraph(\""+QDir::tempPath()+"/autocorrelation"+model+img_idx+".png\" ,file=\""+QDir::tempPath()+"/out.html\")";
    w_->R_.parseEvalQ(eval.toStdString());
    w_->addHtml("Autocorrelation functions");
    w_->tabs_->setCurrentWidget(w_->txtWidget_);
}


// ARIMA
void Statistics::arima() {
    QString model = allModels(true, "ts");

    if (model == "-1") {
        return;
    }


    QDialog dialog(w_);
    dialog.setWindowTitle(QObject::tr("ARIMA"));
    QFormLayout form(&dialog);

    QLineEdit* name = new QLineEdit;
    QLineEdit* p = new QLineEdit;
    QLineEdit* d = new QLineEdit;
    QLineEdit* q = new QLineEdit;

    form.addRow(new QLabel(QObject::tr("ARIMA name:")));
    name->setText("ARIMA"+QString::number(w_->listARIMA_.length() + 1));
    form.addWidget(name);

    form.addRow(new QLabel(QObject::tr("p")));
    p->setText(QString::number(1));
    form.addWidget(p);

    form.addRow(new QLabel(QObject::tr("d")));
    d->setText(QString::number(1));
    form.addWidget(d);

    form.addRow(new QLabel(QObject::tr("q")));
    q->setText(QString::number(1));
    form.addWidget(q);


    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                               Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    QObject::connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));


    QString eval;
    if (dialog.exec() == QDialog::Accepted) {
         if (!name->text().isEmpty() && !p->text().isEmpty() && !d->text().isEmpty() && !q->text().isEmpty()) {
            eval = name->text()+" <- arima("+model+",order=c("+p->text()+","+d->text()+","+q->text()+"))";
            w_->R_.parseEvalQ(eval.toStdString());

            w_->resetFile();
            w_->R_.parseEvalQ("HTML("+name->text().toStdString()+", file=\""+QDir::tempPath().toStdString()+"/out.html\")");
            w_->addHtml("ARIMA: Autoregressive integrated moving average");

            w_->tabs_->setCurrentWidget(w_->txtWidget_);
        }
        else {
            QMessageBox* msgBox = new QMessageBox(w_);
            msgBox->setText(QObject::tr("All fields are needed"));
            msgBox->setStandardButtons(QMessageBox::Ok);
            msgBox->setDefaultButton(QMessageBox::Ok);
            msgBox->exec();
            return;
        }
    }
    else {
        return;
    }


}
