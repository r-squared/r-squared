/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "mainwindow.h"


// Constructor
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {

    QFile styleFile (":/qss/qss/style.css");
    styleFile.open(QFile::ReadOnly);
    QString style( styleFile.readAll());
    this->setStyleSheet(style);


    edit_ = true;
    installDependencies();

    icon_ = QPixmap(":/images/images/logo_mini.png");
    this->setWindowIcon(icon_);

    wgtMain_ = new QWidget(this);
    lytMain_ = new QGridLayout(wgtMain_);
    lytMain_->setMargin(0);
    wgtMain_->setLayout(lytMain_);


    setCentralWidget(wgtMain_);

    // Definimos el tamaño de la ventana inicial
   // this->setGeometry(0, 0, 920, 660);
    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size()*2,
            qApp->desktop()->availableGeometry()
        )
    );
    this->setWindowTitle(tr("R-squared"));


    wgtTabs_ = new QWidget(wgtMain_);
    lytTabs_ = new QGridLayout(wgtTabs_);
    lytTabs_->setMargin(0);
    wgtTabs_->setLayout(lytTabs_);

    // Pestañas
    tabs_ = new QTabWidget(this);
    tabs_->setTabsClosable(true);
    tabs_->setTabPosition(QTabWidget::South);

    totalRows_ = 25;
    gridMain_ = new QTableWidget(totalRows_,15,this);
    txtWidget_ = new editor;
    logTab_ = new QPlainTextEdit;
    logTab_->setReadOnly(true);

    tabs_->addTab(wgtTabs_, tr("Data grid"));
    tabs_->addTab(txtWidget_, tr("Editor"));
    tabs_->addTab(logTab_, tr("Log"));


    lytMain_->addWidget(tabs_,0,0,0,0);

    // Set the table in the first tab layout

    lytTabs_->addWidget(gridMain_,1,0,1,1);

    // Set the command line in the first tab layout
    /*lineCommand_ = new QPlainTextEdit();
    lineCommand_->setMaximumHeight(80);
    lytTabs_->addWidget(lineCommand_,2,0,1,1);*/


    gen_ = new Generic();
    gen_->readConfiguration();

    createMenu();

    createTableToolBar();


    totalCols_ = 0;
    isHelp = false;

    tabs_->tabBar()->tabButton(0, QTabBar::RightSide)->hide();
    tabs_->tabBar()->tabButton(1, QTabBar::RightSide)->hide();
    tabs_->tabBar()->tabButton(2, QTabBar::RightSide)->hide();


    connect(tabs_, SIGNAL(tabCloseRequested(int)), this, SLOT(removeTab(int)));

    if (edit_) {
        connect(gridMain_, SIGNAL(cellChanged(int,int)), this, SLOT(editCell(int, int)));
    }

     R_.parseEvalQ("library(\"R2HTML\")");

     edit_ = true;



}

// Destructor
MainWindow::~MainWindow() {}


// CREATE MENU. Create JSON menu
void MainWindow::createMenuJSON () {

    mnuStatisticJson_ = new QMenu(tr("&Statistics"), this);
    mainMenu_ -> addMenu(mnuStatisticJson_);
    QMap<QString, QMenu*> menu;
    QSignalMapper* mapperJSON = new QSignalMapper(this);

    foreach(QString key,gen_->rf_->keys()) {
       QString submenu = gen_->rf_->value(key).menu;
       if (!menu.contains(submenu)) {
           menu.insert(submenu, mnuStatisticJson_-> addMenu(submenu));
       }
       actStatisticJSON_ = new QAction(gen_->rf_->value(key).title, this);
       menu[submenu]-> addAction(actStatisticJSON_);

       connect(actStatisticJSON_, SIGNAL(triggered()), mapperJSON, SLOT(map()));
       mapperJSON->setMapping(actStatisticJSON_, gen_->rf_->value(key).name);

    };


    connect(mapperJSON,SIGNAL(mapped(QString)),this,SLOT(genericFunction(QString)));
}



// CREATE MENU. Create main menu
void MainWindow::createMenu () {

    mainMenu_ = new QMenuBar(this);


    mnuFile_ = new QMenu(tr("&File"), this);

    mnuData_ = new QMenu(tr("&Data"), this);

    mnuStatistic_ = new QMenu(tr("&Statistics"), this);

    mnuHelp_ = new QMenu(tr("&Help"), this);

    mnuConf_ = new QMenu(tr("&Configuration"), this);

    mainMenu_ -> addMenu(mnuFile_);
    mainMenu_ -> addMenu(mnuData_);
    mainMenu_ -> addMenu(mnuStatistic_);


    // Create JSON menu
    createMenuJSON();

    mainMenu_ ->addMenu(mnuConf_);
    mainMenu_ ->addMenu(mnuHelp_);

    setMenuBar(mainMenu_);


    actFileNew_ = new QAction(QIcon(":/icons/icons/new.png"), tr("&New Project"), this);
    actFileNew_ -> setShortcut(QKeySequence(Qt::CTRL + Qt::Key_N));
    mnuFile_ -> addAction(actFileNew_);

    actFileOpen_ = new QAction(QIcon(":/icons/icons/open.png"), tr("&Open CSV"), this);
    actFileOpen_ -> setShortcut(QKeySequence(Qt::CTRL + Qt::Key_O));
    mnuFile_ -> addAction(actFileOpen_);

    actFileOpenWorkspace_ = new QAction(QIcon(":/icons/icons/open-rdata.png"), tr("&Open RData"), this);
    actFileOpenWorkspace_ -> setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_O));
    mnuFile_ -> addAction(actFileOpenWorkspace_);

    actFileSave_ = new QAction(QIcon(":/icons/icons/document-save.png"), tr("&Save RData"), this);
    actFileSave_ -> setShortcut(QKeySequence(Qt::CTRL + Qt::Key_S));
    mnuFile_ -> addAction(actFileSave_);

    actFileQuit_ = new QAction(QIcon(":/icons/icons/quit.png"), tr("&Exit"), this);
    actFileQuit_ -> setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q));
    mnuFile_ -> addAction(actFileQuit_);

    connect(actFileQuit_, SIGNAL(triggered()), this, SLOT(close()));

    actDataNewVar_ = new QAction(QIcon(":/icons/icons/table-newrow.png"), tr("&New Variable"), this);
    mnuData_ -> addAction(actDataNewVar_);
    connect(actDataNewVar_, SIGNAL(triggered()), this, SLOT(newVariable()));

    actDataEditCell_ = new QAction(QIcon(":/icons/icons/table-cell.png"), tr("&Edit cell"), this);
    mnuData_ -> addAction(actDataEditCell_);
    connect(actDataEditCell_, SIGNAL(triggered()), this, SLOT(editCell()));

    actDataRenameVar_ = new QAction(QIcon(":/icons/icons/table-header.png"), tr("&Rename var"), this);
    mnuData_ -> addAction(actDataRenameVar_);
    actDataRenameVar_ -> setShortcut(QKeySequence(Qt::Key_F2));
    connect(actDataRenameVar_, SIGNAL(triggered()), this, SLOT(renameVar()));

    mnuUnidimensional_ = new QMenu(tr("&Unidimensional"), this);
    mnuStatistic_->addMenu(mnuUnidimensional_);

    mnuBidimensional_ = new QMenu(tr("&Bidimensional"), this);
    mnuStatistic_->addMenu(mnuBidimensional_);

    mnuModels_ = new QMenu(tr("&Fitting models"), this);
    mnuStatistic_->addMenu(mnuModels_);

    mnuTransformed_ = new QMenu(tr("&Transformed"), this);
    mnuStatistic_->addMenu(mnuTransformed_);

    mnuTimeSeries_ = new QMenu(tr("&Time Series"), this);
    mnuStatistic_->addMenu(mnuTimeSeries_);





    actStatisticSummary_ = new QAction(tr("Summary"), this);
    mnuUnidimensional_ -> addAction(actStatisticSummary_);

    connect(actStatisticSummary_, SIGNAL(triggered()), this, SLOT(summary()));

    QSignalMapper* mapper = new QSignalMapper(this);
    QSignalMapper* mapperGr = new QSignalMapper(this);
    QSignalMapper* mapper2 = new QSignalMapper(this);
    QSignalMapper* mapperGr2 = new QSignalMapper(this);
    QSignalMapper* mapperModels = new QSignalMapper(this);
    QSignalMapper* mapperTrans = new QSignalMapper(this);
    QSignalMapper* mapperQQ = new QSignalMapper(this);


    actStatisticMax_ = new QAction(tr("Max"), this);
    mnuUnidimensional_ -> addAction(actStatisticMax_);

    connect(actStatisticMax_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticMax_, tr("max|Max"));

    actStatisticMin_ = new QAction(tr("Min"), this);
    mnuUnidimensional_ -> addAction(actStatisticMin_);

    connect(actStatisticMin_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticMin_, tr("min|Min"));

    actStatisticSum_ = new QAction(tr("Sum"), this);
    mnuUnidimensional_ -> addAction(actStatisticSum_);

    connect(actStatisticSum_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticSum_, tr("sum|Sum"));

    actStatisticCumsum_ = new QAction(tr("Cumulative Sums"), this);
    mnuUnidimensional_ -> addAction(actStatisticCumsum_);

    connect(actStatisticCumsum_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticCumsum_, tr("cumsum|Cumulative Sums"));

    actStatisticCumprod_ = new QAction(tr("Cumulative Products"), this);
    mnuUnidimensional_ -> addAction(actStatisticCumprod_);

    connect(actStatisticCumprod_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticCumprod_, tr("cumprod|Cumulative Products"));

    actStatisticCummax_ = new QAction(tr("Cumulative Max"), this);
    mnuUnidimensional_ -> addAction(actStatisticCummax_);

    connect(actStatisticCummax_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticCummax_, tr("cummax|Cumulative Max"));

    actStatisticCummin_ = new QAction(tr("Cumulative Min"), this);
    mnuUnidimensional_ -> addAction(actStatisticCummin_);

    connect(actStatisticCummin_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticCummin_, tr("cummin|Cumulative Min"));

    actStatisticMad_ = new QAction(tr("Median absolute deviation"), this);
    mnuUnidimensional_ -> addAction(actStatisticMad_);

    connect(actStatisticMad_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticMad_, tr("mad|Median absolute deviation"));

    actStatisticMean_ = new QAction(tr("Mean"), this);
    mnuUnidimensional_ -> addAction(actStatisticMean_);

    connect(actStatisticMean_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticMean_, tr("mean|Mean"));

    actStatisticMedian_ = new QAction(tr("Median"), this);
    mnuUnidimensional_ -> addAction(actStatisticMedian_);

    connect(actStatisticMedian_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticMedian_, tr("median|Median"));

    actStatisticStdDeviation_ = new QAction(tr("Standard deviation"), this);
    mnuUnidimensional_ -> addAction(actStatisticStdDeviation_);

    connect(actStatisticStdDeviation_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticStdDeviation_, tr("sd|Standard Deviation"));

    actStatisticVariance_= new QAction(tr("Variance"), this);
    mnuUnidimensional_ -> addAction(actStatisticVariance_);

    connect(actStatisticVariance_, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(actStatisticVariance_, tr("var|Variance"));

    connect(mapper,SIGNAL(mapped(QString)),this,SLOT(unidimensional(QString)));

    actStatisticHistogram_= new QAction(tr("Histogram"), this);
    mnuUnidimensional_ -> addAction(actStatisticHistogram_);

    connect(actStatisticHistogram_, SIGNAL(triggered()), mapperGr, SLOT(map()));
    mapperGr->setMapping(actStatisticHistogram_, tr("hist|Histogram"));

    actStatisticBoxPlot_= new QAction(tr("Box Plot"), this);
    mnuUnidimensional_ -> addAction(actStatisticBoxPlot_);

    connect(actStatisticBoxPlot_, SIGNAL(triggered()), mapperGr, SLOT(map()));
    mapperGr->setMapping(actStatisticBoxPlot_, tr("boxplot|Box Plot"));

    connect(mapperGr,SIGNAL(mapped(QString)),this,SLOT(unidimensionalGraph(QString)));


    actStatisticCovariance_ = new QAction(tr("Covariance"), this);
    mnuBidimensional_ -> addAction(actStatisticCovariance_);

    connect(actStatisticCovariance_, SIGNAL(triggered()), mapper2, SLOT(map()));
    mapper2->setMapping(actStatisticCovariance_, tr("cov|Covariance"));

    actStatisticCorrelation_ = new QAction(tr("Correlation"), this);
    mnuBidimensional_ -> addAction(actStatisticCorrelation_);

    connect(actStatisticCorrelation_, SIGNAL(triggered()), mapper2, SLOT(map()));
    mapper2->setMapping(actStatisticCorrelation_, tr("cor|Correlation"));

    connect(mapper2,SIGNAL(mapped(QString)),this,SLOT(bidimensional(QString)));


    actStatisticPlot_= new QAction(tr("Scatter Plot"), this);
    mnuBidimensional_ -> addAction(actStatisticPlot_);

    connect(actStatisticPlot_, SIGNAL(triggered()), mapperGr2, SLOT(map()));
    mapperGr2->setMapping(actStatisticPlot_, tr("plot|Scatter Plot"));

    connect(mapperGr2,SIGNAL(mapped(QString)),this,SLOT(bidimensionalGraph(QString)));



    actStatisticFittingModels_ = new QAction(tr("Linear model"), this);
    mnuModels_-> addAction(actStatisticFittingModels_);

    connect(actStatisticFittingModels_, SIGNAL(triggered()), mapperModels, SLOT(map()));
    mapperModels->setMapping(actStatisticFittingModels_, tr("lm|Linear model"));

    connect(mapperModels,SIGNAL(mapped(QString)),this,SLOT(fittingModels(QString)));

    actStatisticAnova_ = new QAction(tr("Anova"), this);
    mnuModels_ -> addAction(actStatisticAnova_);

    connect(actStatisticAnova_, SIGNAL(triggered()), this, SLOT(anova()));

    actStatisticTransformedBoxCox_ = new QAction(tr("BoxCox"), this);
    mnuTransformed_ -> addAction(actStatisticTransformedBoxCox_);

    connect(actStatisticTransformedBoxCox_, SIGNAL(triggered()), this, SLOT(boxCox()));


 /*   actStatisticCurveHist_ = new QAction(tr("Histogram with curve (residuals)"), this);
    mnuModels_ -> addAction(actStatisticCurveHist_);

    connect(actStatisticCurveHist_, SIGNAL(triggered()), this, SLOT(curveHist()));*/

    actStatisticQQ_ = new QAction(tr("Q-Q Plot"), this);
    mnuUnidimensional_ -> addAction(actStatisticQQ_);

    connect(actStatisticQQ_, SIGNAL(triggered()), mapperQQ, SLOT(map()));
    mapperQQ->setMapping(actStatisticQQ_, tr(""));

    actStatisticQQ_ = new QAction(tr("Q-Q Plot (residuals)"), this);
    mnuModels_ -> addAction(actStatisticQQ_);

    connect(actStatisticQQ_, SIGNAL(triggered()), mapperQQ, SLOT(map()));
    mapperQQ->setMapping(actStatisticQQ_, tr("$residuals"));


    connect(mapperQQ,SIGNAL(mapped(QString)),this,SLOT(qqPlot(QString)));

    actStatisticShapiro_ = new QAction(tr("Shapiro-Wilk"), this);
    mnuModels_ -> addAction(actStatisticShapiro_);

    connect(actStatisticShapiro_, SIGNAL(triggered()), this, SLOT(shapiroWilk()));


    actStatisticSkolmogorovSmirnov = new QAction(tr("Kolmogorov-Smirnov"), this);
    mnuModels_ -> addAction(actStatisticSkolmogorovSmirnov);

    connect(actStatisticSkolmogorovSmirnov, SIGNAL(triggered()), this, SLOT(kolmogorovSmirnov()));

    actStatisticdurbinWatson_ = new QAction(tr("Durbin-Watson"), this);
    mnuModels_ -> addAction(actStatisticdurbinWatson_);

    connect(actStatisticdurbinWatson_, SIGNAL(triggered()), this, SLOT(durbinWatson()));



    actStatisticTransformedSquare_ = new QAction(tr("Square"), this);
    mnuTransformed_-> addAction(actStatisticTransformedSquare_);

    connect(actStatisticTransformedSquare_, SIGNAL(triggered()), mapperTrans, SLOT(map()));
    mapperTrans->setMapping(actStatisticTransformedSquare_, tr("Square"));

    actStatisticTransformedExp_ = new QAction(tr("Exponentiation"), this);
    mnuTransformed_-> addAction(actStatisticTransformedExp_);

    connect(actStatisticTransformedExp_, SIGNAL(triggered()), mapperTrans, SLOT(map()));
    mapperTrans->setMapping(actStatisticTransformedExp_, tr("Exponentiation"));

    actStatisticTransformedSquareRoot_ = new QAction(tr("Square root"), this);
    mnuTransformed_-> addAction(actStatisticTransformedSquareRoot_);

    connect(actStatisticTransformedSquareRoot_, SIGNAL(triggered()), mapperTrans, SLOT(map()));
    mapperTrans->setMapping(actStatisticTransformedSquareRoot_, tr("Square root"));

    actStatisticTransformedLog_ = new QAction(tr("Natural logarithm"), this);
    mnuTransformed_-> addAction(actStatisticTransformedLog_);

    connect(actStatisticTransformedLog_, SIGNAL(triggered()), mapperTrans, SLOT(map()));
    mapperTrans->setMapping(actStatisticTransformedLog_, tr("Natural logarithm"));

    actStatisticTransformedLog10_ = new QAction(tr("Base 10 logarithm"), this);
    mnuTransformed_-> addAction(actStatisticTransformedLog10_);

    connect(actStatisticTransformedLog10_, SIGNAL(triggered()), mapperTrans, SLOT(map()));
    mapperTrans->setMapping(actStatisticTransformedLog10_, tr("Base 10 logarithm"));

    actStatisticTransformedInverse_ = new QAction(tr("Reciprocal"), this);
    mnuTransformed_-> addAction(actStatisticTransformedInverse_);

    connect(actStatisticTransformedInverse_, SIGNAL(triggered()), mapperTrans, SLOT(map()));
    mapperTrans->setMapping(actStatisticTransformedInverse_, tr("Reciprocal"));

    actStatisticTransformedInverseSquareRoot_ = new QAction(tr("Reciprocal square root"), this);
    mnuTransformed_-> addAction(actStatisticTransformedInverseSquareRoot_);

    connect(actStatisticTransformedInverseSquareRoot_, SIGNAL(triggered()), mapperTrans, SLOT(map()));
    mapperTrans->setMapping(actStatisticTransformedInverseSquareRoot_, tr("Reciprocal square root"));


    connect(mapperTrans,SIGNAL(mapped(QString)),this,SLOT(transformed(QString)));


    actStatisticTimeSeries_ = new QAction(tr("Time Series"), this);
    mnuTimeSeries_ -> addAction(actStatisticTimeSeries_);

    connect(actStatisticTimeSeries_, SIGNAL(triggered()), this, SLOT(timeSeries()));

    actStatisticTimeSeriesDiff_ = new QAction(tr("Differentiation"), this);
    mnuTimeSeries_ -> addAction(actStatisticTimeSeriesDiff_);

    connect(actStatisticTimeSeriesDiff_, SIGNAL(triggered()), this, SLOT(timeSeriesDiff()));


    actStatisticAutocorrelations_ = new QAction(tr("Autocorrelations"), this);
    mnuTimeSeries_ -> addAction(actStatisticAutocorrelations_);

    connect(actStatisticAutocorrelations_, SIGNAL(triggered()), this, SLOT(autocorrelations()));


    actStatisticArima_ = new QAction(tr("ARIMA (p,d,q)"), this);
    mnuTimeSeries_ -> addAction(actStatisticArima_);

    connect(actStatisticArima_, SIGNAL(triggered()), this, SLOT(arima()));



    actHelpAbout_ = new QAction(QIcon(":/images/images/logo_mini.png"), tr("&About"), this);
    actHelpAbout_ -> setShortcut(QKeySequence::WhatsThis);
    mnuHelp_ -> addAction(actHelpAbout_);

    connect(actHelpAbout_, SIGNAL(triggered()), this, SLOT(about()));

    actHelpHelp_ = new QAction(QIcon(":/images/images/logo_mini.png"), tr("&R-squared help"), this);
    actHelpHelp_ -> setShortcut(QKeySequence::HelpContents);
    mnuHelp_ -> addAction(actHelpHelp_);

    connect(actHelpHelp_, SIGNAL(triggered()), this, SLOT(help()));




    actConfAdd_ = new QAction(QIcon(":/icons/icons/new.png"), tr("&Add configuration"), this);
    mnuConf_ -> addAction(actConfAdd_);

    connect(actConfAdd_, SIGNAL(triggered()), this, SLOT(createConfiguration()));

    actConfload_ = new QAction(QIcon(":/icons/icons/open.png"), tr("&Load configuration"), this);
    mnuConf_ -> addAction(actConfload_);

    connect(actConfload_, SIGNAL(triggered()), this, SLOT(loadFileConfiguration()));

    actRload_ = new QAction(QIcon(":/icons/icons/open-rdata.png"), tr("&Load R functions"), this);
    mnuConf_ -> addAction(actRload_);

    connect(actRload_, SIGNAL(triggered()), this, SLOT(loadFileFunctionsR()));



}




// CREATE TABLE TOOL BAR. Create ToolBar
void MainWindow::createTableToolBar() {

    //tableToolBar_ = addToolBar(tr("Actions"));

    tableToolBar_ = addToolBar(tr("File"));
    tableToolBar_->addAction(actFileNew_);
    tableToolBar_->addAction(actFileOpen_);

    tableToolBar_->addAction(actFileSave_);
    tableToolBar_->addAction(actFileOpenWorkspace_);


    tableToolBar_->addSeparator();

    tableToolBar_->addAction(actDataNewVar_);
    tableToolBar_->addAction(actDataEditCell_);
    tableToolBar_->addAction(actDataRenameVar_);


    connect(actFileOpen_, SIGNAL(triggered()), this, SLOT(openFile()));
    connect(actFileNew_, SIGNAL(triggered()), this, SLOT(newProject()));
    connect(actFileOpenWorkspace_, SIGNAL(triggered()), this, SLOT(loadWorkspace()));
    connect(actFileSave_, SIGNAL(triggered()), this, SLOT(saveWorkspace()));

    lytTabs_->addWidget(tableToolBar_,0,0,1,1);
}

// ABOUT. About R-Squared
void MainWindow::about()
 {


    QMessageBox msgBox(this);

    msgBox.addButton(QMessageBox::Ok);
    msgBox.setText(tr("<b>R-squared project</b> is Free Software released under the GNU/GPLv3 License.<br/> <br/>"
                      "<strong>Source code: </strong><br/><a href=\"https://gitlab.com/r-squared/r-squared\">https://gitlab.com/r-squared/r-squared</a><br/>"
                      "<strong>Web page: </strong><br/><a href=\"http://rsquaredproject.wordpress.com/\">http://rsquaredproject.wordpress.com/</a><br/>"
                      "<br/><br/><br/>"
                      "<strong>Authors:</strong> <br/> Eduardo Nacimiento García <br/>"
                      "Andrés Nacimiento García <br/>"
                      "<strong>Colaborators:</strong> <br/> Holi Díaz Kaas <br/>"
                      "<strong>Version:</strong> <br/> 0.3 alpha - Development"));

    msgBox.setIconPixmap(QPixmap(":/images/images/logo_mini.png"));
    msgBox.setWindowTitle("About R-squared project");
    msgBox.exec();
 }



// ---- FILES MANAGEMENT ----

// OPEN FILE. Open files with .csv format
void MainWindow::openFile() {
    Files *file = new Files(this);
    file->openFile();
}

void MainWindow::newProject(){
    Files *file = new Files(this);
    file->newProject();
}

// LOAD WORKSPACE. Load previous workspace saved
void MainWindow::loadWorkspace() {
    Files *file = new Files(this);
    file->loadWorkspace();
}

// SAVE WORKSPACE. Save the actual workspace you are using
void MainWindow::saveWorkspace() {
    Files *file = new Files(this);
    file->saveWorkspace();

}

// Load Json configuration
void MainWindow::loadFileConfiguration () {
    Files *fl_ = new Files(this);
    fl_->loadFileConfiguration();
    gen_ = new Generic();
    gen_->readConfiguration();
    createMenu();
}


// Load R funtions
void MainWindow::loadFileFunctionsR () {
    Files *fl_ = new Files(this);
    fl_->loadFileFunctionsR();
}

// Create Json configuration
void MainWindow::createConfiguration () {
    DataGrid *dg_ = new DataGrid(this);
    dg_->createConfiguration();
}


// ---- STATISTICS METHODS ----

//
void MainWindow::genericFunction (QString operation) {
    Statistics *stats = new Statistics(this);
    stats->genericFunction(operation, gen_->rf_);
}

// SUMARY. Create a summary in Editor tab
void MainWindow::summary () {

    Statistics *stats = new Statistics(this);
    stats->summary();
}


// UNIDIMENSIONAL. Operations with unidimensional variables
void MainWindow::unidimensional (QString operation) {

    Statistics *stats = new Statistics(this);
    stats->unidimensional(operation);
}

// UNIDIMENSIONAL GRAPH. Operations' graph with unidimensional variables
void MainWindow::unidimensionalGraph (QString operation) {

    Statistics *stats = new Statistics(this);
    stats->unidimensionalGraph(operation);
}

// BIDIMENSIONAL. Operations with bidimensional variables
void MainWindow::bidimensional (QString operation) {

    Statistics *stats = new Statistics(this);
    stats->bidimensional(operation);
}

// BIDIMENSIONAL GRAPH. Operations' graph with bidimensional variables
void MainWindow::bidimensionalGraph (QString operation) {

    Statistics *stats = new Statistics(this);
    stats->bidimensionalGraph(operation);

}

// FITTING MODELS. Generate a new lineal model
void MainWindow::fittingModels (QString operation) {

    Statistics *stats = new Statistics(this);
    stats->fittingModels(operation);
}

// FITTING MODELS. Generate a new lineal model
void MainWindow::anova () {

    Statistics *stats = new Statistics(this);
    stats->anova();
}

// BoxCox Graph
void MainWindow::boxCox () {

    Statistics *stats = new Statistics(this);
    stats->boxCox();
}

// QQ Plot
void MainWindow::qqPlot (QString operation) {

    Statistics *stats = new Statistics(this);
    stats->qqPlot(operation);
}

// Histogram + curve (residuals)
void MainWindow::curveHist () {

    Statistics *stats = new Statistics(this);
    stats->curveHist();
}

// Test: Shapiro-Wilk
void MainWindow::shapiroWilk () {

    Statistics *stats = new Statistics(this);
    stats->shapiroWilk();
}

// Test: Kolmogoroc Smirnov
void MainWindow::kolmogorovSmirnov () {

    Statistics *stats = new Statistics(this);
    stats->kolmogorovSmirnov();
}

// Test: Durbin Watson
void MainWindow::durbinWatson () {

    Statistics *stats = new Statistics(this);
    stats->durbinWatson();
}

// TRANSFORMED from a varible
void MainWindow::transformed(QString operation) {
    Statistics *stats = new Statistics(this);
    stats->transformed(operation);
}

// Time Series
void MainWindow::timeSeries() {
    Statistics *stats = new Statistics(this);
    stats->timeSeries();
}

// Time Series Diff
void MainWindow::timeSeriesDiff() {
    Statistics *stats = new Statistics(this);
    stats->timeSeriesDiff();
}

// Autocorrelation funtions
void MainWindow::autocorrelations() {
    Statistics *stats = new Statistics(this);
    stats->autocorrelations();
}

// ARIMA
void MainWindow::arima() {
    Statistics *stats = new Statistics(this);
    stats->arima();
}


// RESET FILE. Reset HTML file
void MainWindow::resetFile() {
    QFile file(QDir::tempPath()+"/out.html");

    if (file.open(QFile::WriteOnly | QFile::Truncate)) {
        file.close();
    }
}

// ADD HTML. Add to html file
void MainWindow::addHtml(QString title) {
    QFile file(QDir::tempPath()+"/out.html");
    if (file.open(QFile::ReadOnly)) {
     //   QTextDocument *textDocument = txtWidget_->txtEditor_->document();
        QTextCursor cursor = txtWidget_->txtEditor_->textCursor();
        if (title != "") {
            cursor.insertHtml("<strong>"+title+": </strong>");
        }
        txtWidget_->txtEditor_->append(file.readAll());
    }
    file.close();
}

// HELP. Show R-Squared help
void MainWindow::help() {
    if (!isHelp) {
        web_ = new QWebEngineView;
        web_->setUrl(QUrl::fromLocalFile(QFileInfo("help/help.html").absoluteFilePath()));
        isHelp = true;
        tabs_->addTab(web_, tr("Help"));
    }
    tabs_->setCurrentWidget(web_);
}

// REMOVE TAB. Remove a tab
void MainWindow::removeTab (int index) {
    if (index > 1) {
        tabs_->removeTab(index);
        isHelp = false;
    }
}


// INSTALL DEPENDENCIES. Install R dependencies if it's needed
void MainWindow::installDependencies() {

    Rcpp::LogicalVector check1 = R_.parseEval("check<- \"Rcpp\" %in% rownames(installed.packages())");
    if (!check1[0]) {
        R_.parseEvalQ("install.packages(c(\"Rcpp\", repos=\"http://cran.r-project.org\")");
    }
    Rcpp::LogicalVector check2 = R_.parseEval("check<- \"RInside\" %in% rownames(installed.packages())");
    if (!check2[0]) {
        R_.parseEvalQ("install.packages(c(\"RInside\", repos=\"http://cran.r-project.org\")");
    }
    /*Rcpp::LogicalVector check3 = R_.parseEval("check<- \"inline\" %in% rownames(installed.packages())");
    if (!check3[0]) {
        R_.parseEvalQ("install.packages(c(\"inline\", repos=\"http://cran.r-project.org\")");
    }*/
    Rcpp::LogicalVector check4 = R_.parseEval("check<- \"R2HTML\" %in% rownames(installed.packages())");
    if (!check4[0]) {
        R_.parseEvalQ("install.packages(\"R2HTML\", dependencies=TRUE, repos=\"http://cran.r-project.org\")");
    }
    Rcpp::LogicalVector check5 = R_.parseEval("check<- \"R.utils\" %in% rownames(installed.packages())");
    if (!check5[0]) {
        R_.parseEvalQ("install.packages(\"R.utils\", repos=\"http://cran.r-project.org\")");
    }

    R_.parseEvalQ("library(\"R.utils\")");
    R_.parseEvalQ("sourceDirectory(\"Rlocal/\", recursive=TRUE);");


    R_.parseEvalQ("rm(check)");
    R_.parseEvalQ("rm(argv)");
}


// ---- GRID SETTINGS ---

// NEW VARIABLE. Add a new variable
void MainWindow::newVariable () {

    DataGrid *dg_ = new DataGrid(this);
    dg_->newVariable();

}

// RENAME VAR. Rename an existing variable
void MainWindow::renameVar() {
    DataGrid *dg_ = new DataGrid(this);
    dg_->renameVar();

}


// EDIT CELL (1). Edit a Cell called from toolbar
void MainWindow::editCell() {
    DataGrid *dg_ = new DataGrid(this);
    dg_->editCell();

}


// EDIT CELL (2). Edit a Cell called from the grid
void MainWindow::editCell(int row, int col) {
    DataGrid *dg_ = new DataGrid(this);
    dg_->editCell(row, col);

}


// Close:
//   check if documents if changed and ask for save it
//   ask for save the workspace
void MainWindow::closeEvent(QCloseEvent *event) {

    bool next = true;
    QTextDocument * textDocument = txtWidget_->txtEditor_->document();
    if (textDocument->isModified()) {
         QMessageBox msgBoxEditor(this);
         msgBoxEditor.setText(tr("The document was modified"));
         msgBoxEditor.setInformativeText(tr("Do you want to save changes?"));
         msgBoxEditor.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
         msgBoxEditor.setDefaultButton(QMessageBox::Save);
         int ret = msgBoxEditor.exec();
         switch (ret) {
            case QMessageBox::Save:
                txtWidget_->saveDocument();
                break;
             case QMessageBox::Discard:
                 event->accept();
                 break;
            case QMessageBox::Cancel:
                event->ignore();
                next = false;
                break;
            default:
                break;
          }
    }

    if (next) {
        QMessageBox* msgBox = new QMessageBox(this);
        msgBox->setText(tr("Close workspace?"));
        msgBox->setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox->setDefaultButton(QMessageBox::Save);
        int op = msgBox->exec();
        switch (op) {
           case QMessageBox::Save:
               saveWorkspace();
               event->accept();
               break;
           case QMessageBox::Discard:
               event->accept();
               break;
           case QMessageBox::Cancel:
               event->ignore();
               break;
           default:
               event->ignore();
               break;
        }
    }

}


void MainWindow::closeConfTab() {
    DataGrid* dg_ = new DataGrid(this);
    dg_->closeConfTab();

}


void MainWindow::saveConfigurationTab() {
    DataGrid* dg_ = new DataGrid(this);
    dg_->saveConfigurationTab();
}

