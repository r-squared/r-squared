/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "datagrid.h"

// Constructor
DataGrid::DataGrid(MainWindow *w){
    w_ = w;
}

// Destructor
DataGrid::~DataGrid() { }





// Add new user configurations
void DataGrid::createConfiguration() {

    QWidget* configJSON = new QWidget;
    w_->tabs_->addTab(configJSON, QObject::tr("Create configuration"));
    w_->tabs_->setCurrentWidget(configJSON);

    QToolBar* toolBarJSON = new QToolBar();



    QAction* actQuit = new QAction(QIcon(":/icons/icons/quit.png"), QObject::tr("&Discard changes"), configJSON);
    actQuit -> setShortcut(QKeySequence(Qt::CTRL + Qt::Key_O));

    QAction* actSave = new QAction(QIcon(":/icons/icons/document-save.png"), QObject::tr("&Save Configuration"), configJSON);
    actSave -> setShortcut(QKeySequence(Qt::CTRL + Qt::Key_S));

    toolBarJSON->addAction(actQuit);
    toolBarJSON->addAction(actSave);


    QObject::connect(actQuit, SIGNAL(triggered()), w_, SLOT(closeConfTab()));
    QObject::connect(actSave, SIGNAL(triggered()), w_, SLOT(saveConfigurationTab()));




    int maxItems = 5;
    QTableWidget* tableArguments = new QTableWidget(maxItems,5,configJSON);
    QStringList labelsHeader;
    labelsHeader << "value" << "optional" << "type" << "default" << "about";
    tableArguments->setHorizontalHeaderLabels(labelsHeader);

    for (int i = 0; i < maxItems; i++) {
        QComboBox* comboOptional = new QComboBox();
        QComboBox* comboType = new QComboBox();

        QStringList comboList;
        comboList << "TRUE" << "FALSE";
        comboOptional->addItems(comboList);
        tableArguments->setCellWidget(i,1,comboOptional);
        comboList.clear();
        comboList << "object" << "numeric" << "boolean";
        comboType->addItems(comboList);
        tableArguments->setCellWidget(i,2,comboType);
    }


    QGridLayout *form = new QGridLayout;
    QVBoxLayout *topLeftLayout = new QVBoxLayout;
    QGroupBox *tabFunction = new QGroupBox();
    QLabel *nameLabel = new QLabel(QObject::tr("Name"));
    topLeftLayout->addWidget(nameLabel);
    QLineEdit *nameLine = new QLineEdit();
    topLeftLayout->addWidget(nameLine);
    QLabel *titleLabel = new QLabel(QObject::tr("Title"));
    topLeftLayout->addWidget(titleLabel);
    QLineEdit *titleLine = new QLineEdit();
    topLeftLayout->addWidget(titleLine);
    QLabel *descriptionLabel = new QLabel(QObject::tr("Description"));
    topLeftLayout->addWidget(descriptionLabel);
    QPlainTextEdit *descriptionArea = new QPlainTextEdit();
    topLeftLayout->addWidget(descriptionArea);
    QLabel *menuLabel = new QLabel(QObject::tr("Menu"));
    topLeftLayout->addWidget(menuLabel);
    QLineEdit *menuLine = new QLineEdit();
    topLeftLayout->addWidget(menuLine);
    QLabel *categoryLabel = new QLabel(QObject::tr("Category"));
    topLeftLayout->addWidget(categoryLabel);
    QLineEdit *categoryLine = new QLineEdit();
    topLeftLayout->addWidget(categoryLine);

    tabFunction->setLayout(topLeftLayout);

    form->addWidget(toolBarJSON,0,0,1,2);
    form->addWidget(tabFunction, 1, 1,1,1);
    form->addWidget(tableArguments, 1, 2,1,1);


    configJSON->setLayout(form);


}



void DataGrid::closeConfTab() {
    int tabIdx = w_->tabs_->currentIndex();
    QWidget* tabItem = w_->tabs_->widget(tabIdx);
    w_->tabs_->removeTab(tabIdx);
    delete(tabItem);
    w_->tabs_->setCurrentIndex(0);
}



void DataGrid::saveConfigurationTab() {

}

// CHECK COLUMN. Generic
QList<int> DataGrid::CheckColumnGeneric(int size, QVector<RArguments> opRA) {

    QList<int> idx_list;
    if (w_->totalCols_ == 0) {
        idx_list << -1;
        return idx_list;
    }

    int col_num = w_->gridMain_->currentColumn() + 1;
    if (col_num < 1 || col_num > w_->totalCols_) {
        col_num = 0;
    }
    else {
        col_num--;
    }

    QDialog dialog(w_);
    dialog.setWindowTitle(QObject::tr("Select variables"));
    QGridLayout *form = new QGridLayout;
    form->setSizeConstraint(QLayout::SetFixedSize);
   // QFormLayout form(&dialog);


    QGridLayout *topLeftLayout = new QGridLayout;

    QList<QString> varLetters;
    varLetters << "X" << "Y" << "Z" << "U" << "V" << "W";
    QVector<QComboBox*> input;
    for (int i = 0; i < size; i++) {
        QComboBox* inputX = new QComboBox;
        inputX->addItems(w_->listHeader_);
        topLeftLayout->addWidget(new QLabel(QObject::tr("Variable ")+ varLetters.at(i)),i,0);
        topLeftLayout->addWidget(inputX,i,1);
        input.push_back(inputX);
    }

    QPushButton *moreButton = new QPushButton(QObject::tr("&More"));

    moreButton->setCheckable(true);

    QDialogButtonBox *buttonBox = new QDialogButtonBox(Qt::Vertical);

    buttonBox->addButton(QDialogButtonBox::Ok);
    buttonBox->addButton(QDialogButtonBox::Cancel);
    buttonBox->addButton(moreButton, QDialogButtonBox::ActionRole);

    QObject::connect(buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));


    QWidget *extension = new QWidget;
    QObject::connect(moreButton, SIGNAL(toggled(bool)), extension, SLOT(setVisible(bool)));

    QVBoxLayout *extensionLayout = new QVBoxLayout;
    extensionLayout->setMargin(0);

    foreach (RArguments arg, opRA) {
        if (arg.type.toStdString() != "object") {
            QLabel *caseLabel = new QLabel(arg.value);
            extensionLayout->addWidget(caseLabel);
            if (arg.type.toStdString() == "numeric") {

                QSpinBox *caseNum = new QSpinBox();
                caseNum->setValue(arg.isdefault.toInt());
                extensionLayout->addWidget(caseNum);
            }
            else if (arg.type.toStdString() == "boolean") {
                QComboBox *caseBool = new QComboBox();
                caseBool->addItem(QObject::tr("FALSE"));
                caseBool->addItem(QObject::tr("TRUE"));
                if (arg.isdefault == "FALSE") {
                    caseBool->setCurrentIndex(caseBool->findText("FALSE"));
                }
                else {
                    caseBool->setCurrentIndex(caseBool->findText("TRUE"));
                }
                extensionLayout->addWidget(caseBool);
            }
            else {
                QLineEdit *caseLine = new QLineEdit(arg.isdefault);
                extensionLayout->addWidget(caseLine);
            }

        }
    }


    extension->setLayout(extensionLayout);
    extension->hide();

    form->addLayout(topLeftLayout, 0, 0);
    form->addWidget(buttonBox, 0, 1);
    form->addWidget(extension, 1, 0, 1, 2);
    dialog.setLayout(form);

    if (dialog.exec() == QDialog::Accepted) {
        foreach (QComboBox* item, input) {
            idx_list << item->currentIndex();
        }

        return idx_list;
    }
    else {
        idx_list << -1;
        return idx_list;
    }

}

// CHECK COLUMN. Unidimensional
int DataGrid::CheckColumn(int type) {


    if (w_->totalCols_ == 0) {
        return -1;
    }

    int col_num = w_->gridMain_->currentColumn() + 1;

    if (col_num < 1 || col_num > w_->totalCols_) {
        col_num = 0;
    }
    else {
        col_num--;
    }

    bool ok;
    QList<QString> listVar = w_->listHeader_;
    if (type == 0) {
        listVar.push_back(QObject::tr("SELECT ALL VARS"));
    }
    QString item = QInputDialog::getItem(w_, QObject::tr("Select a variable"),
                                         QObject::tr("Variable:"), listVar, col_num, false, &ok);

    if (ok && !item.isEmpty()) {
        if (listVar.indexOf(item) >= w_->listHeader_.size()) {
            return -2;
        }
        else {
            return w_->listHeader_.indexOf(item);
        }
    }
    else {
        return -1;
    }

}

// CHECK COLUMN. Bidimensional
QList<int> DataGrid::CheckColumn2() {

    QList<int> idx_list;
    if (w_->totalCols_ == 0) {
        idx_list << -1;
        return idx_list;
    }

    int col_num = w_->gridMain_->currentColumn() + 1;
    if (col_num < 1 || col_num > w_->totalCols_) {
        col_num = 0;
    }
    else {
        col_num--;
    }

    QDialog dialog(w_);
    dialog.setWindowTitle(QObject::tr("Select variables"));
    QFormLayout form(&dialog);
    QComboBox* inputX = new QComboBox;
    inputX->addItems(w_->listHeader_);
    QComboBox* inputY = new QComboBox;
    inputY->addItems(w_->listHeader_);

    form.addRow(new QLabel(QObject::tr("X variable:")));
    form.addWidget(inputX);
    form.addRow(new QLabel(QObject::tr("Y variable:")));
    form.addWidget(inputY);

    QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                               Qt::Horizontal, &dialog);
    form.addRow(&buttonBox);
    QObject::connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));


    if (dialog.exec() == QDialog::Accepted) {
        idx_list << inputX->currentIndex() << inputY->currentIndex();
        return idx_list;
    }
    else {
        idx_list << -1;
        return idx_list;
    }

}


// NEW VARIABLE. Add a new variable
void DataGrid::newVariable() {

    w_->totalCols_++;

    bool ok;
    QString text = QInputDialog::getText(w_, QObject::tr("New variable"),
                                         QObject::tr("Var name:"), QLineEdit::Normal,
                                         "Var"+QString::number(w_->totalCols_), &ok);
    if (ok && !text.isEmpty()) {

        if (w_->totalCols_ == 1) {
            int item = QInputDialog::getInt(w_, QObject::tr("Number of rows"),
                                                 QObject::tr("Number:"), 1, 1, 2147483647, 1, &ok);

            QString num = QString::number(item);
            if (ok) {
                w_->R_.parseEvalQ("temprow <- matrix(c(rep.int(NA,"+num.toStdString()+")),nrow="+num.toStdString()+",ncol=1)");
                w_->R_.parseEvalQ("mydata <- data.frame(temprow)");
                w_->R_.parseEvalQ("rm(temprow)");
            }
            else {
                w_->totalCols_--;
                return;
            }
        }
        else {
            w_->R_.parseEvalQ("mydata[,"+QString::number(w_->totalCols_).toStdString()+"] <- NA");
        }

        w_->R_.parseEvalQ("names(mydata)["+QString::number(w_->totalCols_).toStdString()+"] <-\""+text.toStdString()+"\"");
        SEXP ans;
        ans = w_->R_.parseEval("mydata[,"+QString::number(w_->totalCols_).toStdString()+"]");
        Rcpp::DataFrame DF(ans);

        QVector<Rcpp::StringVector> dataVector;

        ans = w_->R_.parseEval("names(mydata)");
        Rcpp::CharacterVector names(ans);


        Rcpp::StringVector tmpDF = DF[0];
        dataVector.append(tmpDF);
        w_->listHeader_ << (QString)names[w_->totalCols_ - 1];

        for (int row = 0; row < dataVector[0].size(); row++) {
            w_->gridMain_->setItem(row, w_->totalCols_ -1, new QTableWidgetItem ((QString)dataVector[0][row]));
        }
        w_->gridMain_->setHorizontalHeaderLabels(w_->listHeader_);

    }
    else {
        w_->totalCols_--;
    }

}

// RENAME VAR. Rename an existing variable
void DataGrid::renameVar(){

    if (w_->listHeader_.isEmpty()) {
        return;
    }

    int col = w_->gridMain_->currentColumn() + 1;
    if (col > w_->totalCols_) {
        return;
    }

    bool ok;
    QString text = QInputDialog::getText(w_, QObject::tr("Rename varname"),
                                         QObject::tr("Name:"), QLineEdit::Normal,
                                         w_->listHeader_[col- 1], &ok);

    if (ok) {
        w_->listHeader_[col-1] = text;

        w_->R_.parseEvalQ("names(mydata)["+QString::number(col).toStdString()+"]<-\""+text.toStdString()+"\"");
        w_->gridMain_->setHorizontalHeaderLabels(w_->listHeader_);
    }

}

// EDIT CELL. Edit a Cell called from toolbar
void DataGrid::editCell() {

    int col = w_->gridMain_->currentColumn() + 1;
    int row = w_->gridMain_->currentRow() + 1;

    if (col < 1 || row < 1) {
        return;
    }

    if (col > w_->totalCols_) {
        return;
    }
    Rcpp::StringVector content = w_->R_.parseEval("mydata["+QString::number(row).toStdString()+","+QString::number(col).toStdString()+"]");

    bool ok;
    QString text = QInputDialog::getText(w_, QObject::tr("Edit cell content"),
                                         QObject::tr("Content of")+" ["+QString::number(row)+","+QString::number(col)+"]", QLineEdit::Normal,
                                         (QString)content[0], &ok);

    if (ok) {
        if (text == "inf") {
            return;
        }
        else if (text.toDouble()) {
            w_->R_.parseEvalQ("mydata["+QString::number(row).toStdString()+","+QString::number(col).toStdString()+"] <- "+text.toStdString());
            w_->gridMain_->setItem(row - 1, col - 1, new QTableWidgetItem (text));
        }
    }

}

// EDIT CELL (2). Edit a Cell called from the grid
void DataGrid::editCell(int row, int col){

    if (!w_->edit_) {
        return;
    }

    QString text = w_->gridMain_->item(row, col)->text();

    int tmpCols = w_->totalCols_;
    // Numeric data
    if (text.toDouble())  {
        // No Vars
        if (w_->totalCols_ == 0) {
            // Col != first column
            // Clear cell and return
            if (col > 0) {
                w_->edit_ = false;
                w_->gridMain_->setItem(row, col, new QTableWidgetItem (""));
                w_->edit_ = true;
                return;
            }
            // Col == first column
            // Create var and set name
            w_->R_.parseEvalQ("temprow <- matrix(c(rep.int(NA,"+QString::number(row + 1).toStdString()+")),nrow="+QString::number(row + 1).toStdString()+",ncol=1)");
            w_->R_.parseEvalQ("mydata <- data.frame(temprow)");
            w_->R_.parseEvalQ("rm(temprow)");
            w_->totalCols_++;
            QString tmp_name = "V" + QString::number(col + 1);
            w_->listHeader_ << tmp_name;
            renameVar();
        }
        // Not contiguous column
        // Clear cell and return
        else if (col > (w_->totalCols_)) {
            w_->edit_ = false;
            w_->gridMain_->setItem(row, col, new QTableWidgetItem (""));
            w_->edit_ = true;
            return;
        }
        // Insert data in the var
        w_->R_.parseEvalQ("mydata["+QString::number(row + 1).toStdString()+","+QString::number(col + 1).toStdString()+"] <- "+text.toStdString());
        // New column
        // Rename new vars (col != first column)
        if (col == (tmpCols) && col != 0) {
            w_->totalCols_++;
            QString tmp_name = "V" + QString::number(col + 1);
            w_->listHeader_ << tmp_name;
            renameVar();
        }
        if ((row + 1) == w_->totalRows_) {
            w_->gridMain_->insertRow(row + 1);
            w_->totalRows_++;
        }
    }
    // Not numeric data
    // Clear cell and return
    else {
        w_->edit_ = false;
        w_->gridMain_->setItem(row, col, new QTableWidgetItem (""));
        w_->edit_ = true;
    }

}
