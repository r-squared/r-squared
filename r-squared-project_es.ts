<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="44"/>
        <source>R-squared</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="62"/>
        <source>Data grid</source>
        <translation>Datos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="63"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="105"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="107"/>
        <source>&amp;Data</source>
        <translation>&amp;Datos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="109"/>
        <source>&amp;Statistics</source>
        <translation>&amp;Estadística</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>&amp;New Project</source>
        <translation>&amp;Nuevo Proyecto</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="124"/>
        <source>&amp;Open CSV</source>
        <translation>&amp;Abrir CSV</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="128"/>
        <source>&amp;Open RData</source>
        <translation>&amp;Abrir RData</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="132"/>
        <source>&amp;Save RData</source>
        <translation>&amp;Guardar RData</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="136"/>
        <source>&amp;Exit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="142"/>
        <source>&amp;New Variable</source>
        <translation>&amp;Nueva Variable</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="146"/>
        <source>&amp;Edit cell</source>
        <translation>&amp;Editar celda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="150"/>
        <source>&amp;Rename var</source>
        <translation>&amp;Renombrar variable</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>&amp;Unidimensional</source>
        <translation>&amp;Unidimensional</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <source>&amp;Bidimensional</source>
        <translation>&amp;Bidimensional</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="161"/>
        <source>&amp;Fitting models</source>
        <translation>&amp;Modelos de ajuste</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>&amp;Transformed</source>
        <translation>&amp;Transformado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="167"/>
        <source>&amp;Time Series</source>
        <translation>&amp;Series de Tiempo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="174"/>
        <source>Summary</source>
        <translation>Resumen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Max</source>
        <translation>Máximo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="192"/>
        <source>max|Max</source>
        <translation>max|Máximo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="194"/>
        <source>Min</source>
        <translation>Mínimo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="198"/>
        <source>min|Min</source>
        <translation>min|Mínimo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="200"/>
        <source>Sum</source>
        <translation>Suma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="204"/>
        <source>sum|Sum</source>
        <translation>sum|Suma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="206"/>
        <source>Cumulative Sums</source>
        <translation>Sumas Acumuladas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="210"/>
        <source>cumsum|Cumulative Sums</source>
        <translation>cumsum|Sumas Acumuladas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <source>Cumulative Products</source>
        <translation>Productos Acumulados</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="216"/>
        <source>cumprod|Cumulative Products</source>
        <translation>cumprod|Productos Acumulados</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="218"/>
        <source>Cumulative Max</source>
        <translation>Máximo Acumulado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="222"/>
        <source>cummax|Cumulative Max</source>
        <translation>cummax|Máximo Acumulado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="224"/>
        <source>Cumulative Min</source>
        <translation>Mínimo Acumulado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="228"/>
        <source>cummin|Cumulative Min</source>
        <translation>cummin|Mínimo Acumulado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="230"/>
        <source>Median absolute deviation</source>
        <translation>Desviación absoluta mediana</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="234"/>
        <source>mad|Median absolute deviation</source>
        <translation>mad|Desviación absoluta mediana</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="236"/>
        <source>Mean</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="240"/>
        <source>mean|Mean</source>
        <translation>mean|Media</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="242"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="246"/>
        <source>median|Median</source>
        <translation>median|Mediana</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="248"/>
        <source>Standard deviation</source>
        <translation>Desviación Estandar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="252"/>
        <source>sd|Standard Deviation</source>
        <translation>sd|Desviación Estandar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="254"/>
        <source>Variance</source>
        <translation>Varianza</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="258"/>
        <source>var|Variance</source>
        <translation>var|Varianza</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Histogram</source>
        <translation>Histograma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <source>hist|Histogram</source>
        <translation>hist|Histograma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="268"/>
        <source>Box Plot</source>
        <translation>Diagrama de Caja</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="272"/>
        <source>boxplot|Box Plot</source>
        <translation>boxplot|Diagrama de caja</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Covariance</source>
        <translation>Covarianza</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="281"/>
        <source>cov|Covariance</source>
        <translation>cov|Covarianza</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="283"/>
        <source>Correlation</source>
        <translation>Correlación</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="287"/>
        <source>cor|Correlation</source>
        <translation>cor|Correlacion</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="292"/>
        <source>Scatter Plot</source>
        <translation>Diagrama de dispersión</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="296"/>
        <source>plot|Scatter Plot</source>
        <translation>plot|Diagrama de dispersión</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="302"/>
        <source>Linear model</source>
        <translation>Modelo lineal</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="306"/>
        <source>lm|Linear model</source>
        <translation>lm|Modelo lineal</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="310"/>
        <source>Anova</source>
        <translation>Anova</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="315"/>
        <source>BoxCox</source>
        <translation>BoxCox</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="326"/>
        <source>Q-Q Plot</source>
        <translation>Gráfica Q-Q</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="332"/>
        <source>Q-Q Plot (residuals)</source>
        <translation>Gráfica Q-Q (residuales)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="336"/>
        <source>$residuals</source>
        <translation>$residuals</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="341"/>
        <source>Shapiro-Wilk</source>
        <translation>Shapiro-Wilk</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="347"/>
        <source>Kolmogorov-Smirnov</source>
        <translation>Kolmogorov-Smirnov</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="352"/>
        <source>Durbin-Watson</source>
        <translation>Durbin-Watson</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <location filename="mainwindow.cpp" line="363"/>
        <source>Square</source>
        <translation>Cuadrado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="365"/>
        <location filename="mainwindow.cpp" line="369"/>
        <source>Exponentiation</source>
        <translation>Exponenciación</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="371"/>
        <location filename="mainwindow.cpp" line="375"/>
        <source>Square root</source>
        <translation>Raíz Cuadrada</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="377"/>
        <location filename="mainwindow.cpp" line="381"/>
        <source>Natural logarithm</source>
        <translation>Logaritmo Natural</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="383"/>
        <location filename="mainwindow.cpp" line="387"/>
        <source>Base 10 logarithm</source>
        <translation>Logaritmo Base 10</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="389"/>
        <location filename="mainwindow.cpp" line="393"/>
        <source>Reciprocal</source>
        <translation>Recíproco</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="395"/>
        <location filename="mainwindow.cpp" line="399"/>
        <source>Reciprocal square root</source>
        <translation>Raíz cuadrada recíproca</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="405"/>
        <source>Time Series</source>
        <translation>Series Temporales</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="410"/>
        <source>Differentiation</source>
        <translation>Diferenciación</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="416"/>
        <source>Autocorrelations</source>
        <translation>Autocorrelaciones</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="422"/>
        <source>ARIMA (p,d,q)</source>
        <translation>ARIMA (p,d,q)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="429"/>
        <source>&amp;About</source>
        <translation>&amp;Acerca de</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="435"/>
        <source>&amp;R-squared help</source>
        <translation>&amp;Ayuda R-squared</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="447"/>
        <source>Actions</source>
        <translation>Acciones</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="449"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="480"/>
        <source>&lt;b&gt;R-squared project&lt;/b&gt; is Free Software released under the GNU/GPLv3 License.&lt;br/&gt; &lt;br/&gt;&lt;strong&gt;Source code: &lt;/strong&gt;&lt;br/&gt;&lt;a href=&quot;https://gitlab.com/r-squared/r-squared&quot;&gt;https://gitlab.com/r-squared/r-squared&lt;/a&gt;&lt;br/&gt;&lt;strong&gt;Web page: &lt;/strong&gt;&lt;br/&gt;&lt;a href=&quot;http://rsquaredproject.wordpress.com/&quot;&gt;http://rsquaredproject.wordpress.com/&lt;/a&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;strong&gt;Authors:&lt;/strong&gt; &lt;br/&gt; Eduardo Nacimiento García &lt;br/&gt;Andrés Nacimiento García &lt;br/&gt;&lt;strong&gt;Colaborators:&lt;/strong&gt; &lt;br/&gt; Holi Díaz Kaas &lt;br/&gt;&lt;strong&gt;Version:&lt;/strong&gt; &lt;br/&gt; 0.2 alpha - Development</source>
        <oldsource>&lt;b&gt;R-squared project&lt;/b&gt; is Free Software released under the GNU/GPLv3 License.&lt;br/&gt; &lt;br/&gt;&lt;strong&gt;Source code: &lt;/strong&gt;&lt;br/&gt;&lt;a href=&quot;https://gitorious.org/r-squared&quot;&gt;https://gitorious.org/r-squared&lt;/a&gt;&lt;br/&gt;&lt;strong&gt;Web page: &lt;/strong&gt;&lt;br/&gt;&lt;a href=&quot;http://rsquaredproject.wordpress.com/&quot;&gt;http://rsquaredproject.wordpress.com/&lt;/a&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;strong&gt;Authors:&lt;/strong&gt; &lt;br/&gt; Eduardo Nacimiento García &lt;br/&gt;Andrés Nacimiento García &lt;br/&gt;&lt;strong&gt;Version:&lt;/strong&gt; &lt;br/&gt; 0.2 alpha - Development</oldsource>
        <translation>&lt;b&gt;R-squared project&lt;/b&gt; es Software Libre licenciado bajo la licencia GNU/GPLv3.&lt;br/&gt; &lt;br/&gt;&lt;strong&gt;Código fuente: &lt;/strong&gt;&lt;br/&gt;&lt;a href=&quot;https://gitlab.com/r-squared/r-squared&quot;&gt;https://gitlab.com/r-squared/r-squared&lt;/a&gt;&lt;br/&gt;&lt;strong&gt;Página web: &lt;/strong&gt;&lt;br/&gt;&lt;a href=&quot;http://rsquaredproject.wordpress.com/&quot;&gt;http://rsquaredproject.wordpress.com/&lt;/a&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;strong&gt;Autores:&lt;/strong&gt; &lt;br/&gt; Eduardo Nacimiento García &lt;br/&gt;Andrés Nacimiento García &lt;br/&gt;&lt;strong&gt;Colaboradoras:&lt;/strong&gt; &lt;br/&gt; Holi Díaz Kaas &lt;br/&gt;&lt;strong&gt;Versión:&lt;/strong&gt; &lt;br/&gt; 0.2 alpha - Development</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="680"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="764"/>
        <source>The document was modified</source>
        <translation>El documento fue modificado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="765"/>
        <source>Do you want to save changes?</source>
        <translation>Quieres guardar los cambios?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="787"/>
        <source>Close workspace?</source>
        <translation>Cerrar espacio de trabajo?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="datagrid.cpp" line="52"/>
        <source>SELECT ALL VARS</source>
        <translation>SELECIONAR TODAS LAS VARIABLES</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="54"/>
        <source>Select a variable</source>
        <translation>Seleciona una variable</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="55"/>
        <source>Variable:</source>
        <translation>Variable:</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="89"/>
        <source>Select variables</source>
        <translation>Selecciona variables</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="96"/>
        <source>X variable:</source>
        <translation>Variable X:</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="98"/>
        <source>Y variable:</source>
        <translation>Variable Y:</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="126"/>
        <source>New variable</source>
        <translation>Nueva variable</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="127"/>
        <source>Var name:</source>
        <translation>Nombre de la Variable:</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="132"/>
        <source>Number of rows</source>
        <translation>Número de filas</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="133"/>
        <source>Number:</source>
        <translation>Número:</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="190"/>
        <source>Rename varname</source>
        <translation>Renombrar variable</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="191"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="219"/>
        <source>Edit cell content</source>
        <translation>Editar contenido de la celda</translation>
    </message>
    <message>
        <location filename="datagrid.cpp" line="220"/>
        <source>Content of</source>
        <translation>Contenido de </translation>
    </message>
    <message>
        <location filename="files.cpp" line="40"/>
        <source>Open file CSV</source>
        <translation>Abrir archivo CSV</translation>
    </message>
    <message>
        <location filename="files.cpp" line="42"/>
        <source>Files csv (*.csv)</source>
        <translation>Archivos csv (*.csv)</translation>
    </message>
    <message>
        <location filename="files.cpp" line="92"/>
        <source>Open workspace</source>
        <translation>Abrir espacio de trabajo</translation>
    </message>
    <message>
        <location filename="files.cpp" line="94"/>
        <location filename="files.cpp" line="143"/>
        <source>Files RData (*.RData)</source>
        <translation>Archivos RData (*.RData)</translation>
    </message>
    <message>
        <location filename="files.cpp" line="141"/>
        <source>Save workspace</source>
        <translation>Guardar espacio de trabajo</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="205"/>
        <source>New model</source>
        <translation>Nuevo modelo</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="206"/>
        <location filename="statistics.cpp" line="682"/>
        <source>Model name:</source>
        <translation>Nombre del modelo:</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="293"/>
        <source>Exponent</source>
        <translation>Exponente</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="294"/>
        <source>n:</source>
        <translation>n:</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="408"/>
        <location filename="statistics.cpp" line="412"/>
        <location filename="statistics.cpp" line="416"/>
        <source>Select a model</source>
        <translation>Seleccionar un modelo</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="409"/>
        <source>Models:</source>
        <translation>Modelos:</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="413"/>
        <source>ARIMA:</source>
        <translation>ARIMA:</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="417"/>
        <source>Time Series:</source>
        <translation>Series Temporales:</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="608"/>
        <source>Time Series</source>
        <translation>Series Temporales</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="615"/>
        <source>Time serie name:</source>
        <translation>Nombre de la serie temporal:</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="619"/>
        <source>Start from: (e.g: 1983)</source>
        <translation>Empezar desde: (ej. 1983)</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="623"/>
        <source>Frequency: (e.g: 12)</source>
        <translation>Frecuencia: (ej:12)</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="657"/>
        <location filename="statistics.cpp" line="788"/>
        <source>All fields are needed</source>
        <translation>Todos los archivos son necesarios</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="681"/>
        <source>New Time Serie Differentiation</source>
        <translation>Nueva Diferenciación de Serie Temporal</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="742"/>
        <source>ARIMA</source>
        <translation>ARIMA</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="750"/>
        <source>ARIMA name:</source>
        <translation>Nombre ARIMA:</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="754"/>
        <source>p</source>
        <translation>p</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="758"/>
        <source>d</source>
        <translation>d</translation>
    </message>
    <message>
        <location filename="statistics.cpp" line="762"/>
        <source>q</source>
        <translation>q</translation>
    </message>
</context>
<context>
    <name>editor</name>
    <message>
        <location filename="editor.cpp" line="43"/>
        <source>&amp;Open</source>
        <translation>&amp;Abrir</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="45"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="47"/>
        <source>&amp;Save as PDF</source>
        <translation>&amp;Guardar como PDF</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="50"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copiar</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="53"/>
        <source>&amp;Paste</source>
        <translation>&amp;Pegar</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="56"/>
        <source>&amp;Cut</source>
        <translation>&amp;Cortar</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="59"/>
        <source>&amp;Undo</source>
        <translation>&amp;Deshacer</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="62"/>
        <source>&amp;Redo</source>
        <translation>&amp;Rehacer</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="66"/>
        <source>&amp;Insert table</source>
        <translation>&amp;Insertar tabla</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="68"/>
        <source>&amp;Insert image</source>
        <translation>&amp;Insertar imagen</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="70"/>
        <source>&amp;Fonts</source>
        <translation>&amp;Fuentes</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="72"/>
        <source>&amp;Bold</source>
        <translation>&amp;Negrita</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="74"/>
        <source>&amp;Italic</source>
        <translation>&amp;Cursiva</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="76"/>
        <source>&amp;Underline</source>
        <translation>&amp;Subrayado</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="78"/>
        <source>&amp;Center</source>
        <translation>&amp;Centrar</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="80"/>
        <source>&amp;Left</source>
        <translation>&amp;Izquierda</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="82"/>
        <source>&amp;Right</source>
        <translation>&amp;Derecha</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="84"/>
        <source>&amp;Justify</source>
        <translation>&amp;Justificar</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="87"/>
        <source>Editor toolbar</source>
        <translation>Barra de herramientas del editor</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="225"/>
        <source>Save file html</source>
        <translation>Guardar archivo html</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="227"/>
        <location filename="editor.cpp" line="264"/>
        <source>Files html (*.html)</source>
        <translation>Archivos html.(*.html)</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="262"/>
        <source>Open file html</source>
        <translation>Abrir archivo html</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="307"/>
        <location filename="editor.cpp" line="308"/>
        <source>Size of the table</source>
        <translation>Tamaño de la tabla</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="320"/>
        <source>Select an image</source>
        <translation>Seleccionar una imagen</translation>
    </message>
    <message>
        <location filename="editor.cpp" line="321"/>
        <source>PNG (*.png)
JPEG (*.jpg *jpeg)
GIF (*.gif)
Bitmap Files (*.bmp)
</source>
        <translation>PNG (*.png)
JPEG (*.jpg *jpeg)
GIF (*.gif)
Bitmap Files (*.bmp)
</translation>
    </message>
</context>
</TS>
