/*

    «Copyright 2014 Eduardo Nacimiento, Andres Nacimiento»

    This file is part of R-squared project.

    R-squared project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R-squared project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with R-squared project.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Local libraries
#include "editor.h"
#include "files.h"
#include "statistics.h"
#include "datagrid.h"
#include "generic.h"

// Global libraries
#include <QMainWindow>
#include <QTabWidget>
#include <QGridLayout>
#include <QTextEdit>
#include <QTableWidget>
#include <QMenu>
#include <QMenuBar>
#include <QDockWidget>
#include <QPlainTextEdit>
#include <QToolBar>
#include <QAction>
#include <QMessageBox>
#include <QVector>
#include <QTextTable>
#include <QTextTableCell>
#include <QWebEngineView>
#include <QFile>
#include <QFileInfo>
#include <QSignalMapper>
#include <QDir>
#include <QFormLayout>
#include <QLabel>
#include <QDialogButtonBox>
#include <ctime>
#include <QHeaderView>
#include <QPushButton>
#include <QDebug>
#include <QStyle>
#include <QDesktopWidget>
#include <QApplication>

#include <iostream>

// R libraries
#include <RInside.h>

class RFunctions;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void resetFile();
    void addHtml(QString title);

private slots:

    void installDependencies();

    void createMenu();
    void createMenuJSON();
    void createTableToolBar();
    void createConfiguration();
    void loadFileConfiguration();
    void loadFileFunctionsR();
    void about();
    void help();

    // Files slots
    void openFile();
    void newProject();
    void saveWorkspace();
    void loadWorkspace();

    void removeTab(int index);
    void newVariable();
    void editCell();
    void editCell(int row, int col);
    void renameVar();

    // Tools
    void genericFunction (QString operation);
    void summary();
    void unidimensional (QString operation);
    void unidimensionalGraph (QString operation);
    void bidimensional (QString operation);
    void bidimensionalGraph (QString operation);
    void fittingModels (QString operation);
    void transformed(QString operation);
    void anova();
    void boxCox();
    void qqPlot(QString operation);
    void curveHist();
    void shapiroWilk();
    void kolmogorovSmirnov();
    void durbinWatson();

    void timeSeries();
    void timeSeriesDiff();
    void autocorrelations();
    void arima();


    void closeEvent(QCloseEvent *event);

    void closeConfTab();
    void saveConfigurationTab();


public:

    QWidget *wgtMain_;
    QGridLayout *lytMain_;
    QWidget *wgtTabs_;
    QPlainTextEdit *logTab_;
    QGridLayout *lytTabs_;
    QTabWidget *tabs_;
    QTableWidget *gridMain_;
    QPlainTextEdit* lineCommand_;
    QWebEngineView* web_;

    Generic* gen_;

    QMenuBar* mainMenu_;
    QMenu* mnuFile_;
    QMenu* mnuHelp_;
    QMenu* mnuConf_;
    QMenu* mnuStatistic_;
    QMenu* mnuStatisticJson_;
    QMenu* mnuUnidimensional_;
    QMenu* mnuBidimensional_;
    QMenu* mnuModels_;
    QMenu* mnuData_;
    QMenu* mnuTransformed_;
    QMenu* mnuTimeSeries_;

    QToolBar* fileToolBar_;
    QAction* actFileOpen_;
    QAction* actFileNew_;
    QAction* actFileOpenWorkspace_;
    QAction* actFileSave_;
    QAction* actFileQuit_;
    QAction* actHelpAbout_;
    QAction* actHelpHelp_;

    QAction* actConfAdd_;
    QAction* actConfload_;
    QAction* actRload_;

    QAction* actDataNewVar_;
    QAction* actDataEditCell_;
    QAction* actDataRenameVar_;

    QAction* actStatisticJSON_;
    QAction* actStatisticSummary_;
    QAction* actStatisticMax_;
    QAction* actStatisticMin_;
    QAction* actStatisticSum_;
    QAction* actStatisticMean_;
    QAction* actStatisticMedian_;
    QAction* actStatisticCumsum_;
    QAction* actStatisticCumprod_;
    QAction* actStatisticCummax_;
    QAction* actStatisticCummin_;
    QAction* actStatisticMad_;
    QAction* actStatisticStdDeviation_;
    QAction* actStatisticVariance_;
    QAction* actStatisticHistogram_;
    QAction* actStatisticBoxPlot_;
    QAction* actStatisticCovariance_;
    QAction* actStatisticCorrelation_;
    QAction* actStatisticPlot_;
    QAction* actStatisticFittingModels_;
    QAction* actStatisticAnova_;
    QAction* actStatisticShapiro_;
    QAction* actStatisticQQ_;
    QAction* actStatisticCurveHist_;
    QAction* actStatisticSkolmogorovSmirnov;
    QAction* actStatisticdurbinWatson_;
    QAction* actStatisticTransformedSquare_;
    QAction* actStatisticTransformedExp_;
    QAction* actStatisticTransformedSquareRoot_;
    QAction* actStatisticTransformedLog_;
    QAction* actStatisticTransformedLog10_;
    QAction* actStatisticTransformedInverse_;
    QAction* actStatisticTransformedInverseSquareRoot_;
    QAction* actStatisticTransformedBoxCox_;
    QAction* actStatisticTimeSeries_;
    QAction* actStatisticTimeSeriesDiff_;
    QAction* actStatisticAutocorrelations_;
    QAction* actStatisticArima_;

    QToolBar* tableToolBar_;
    QStringList listHeader_;
    QStringList listModels_;
    QStringList listARIMA_;
    QStringList listTimeSeries_;


    editor* txtWidget_;

    QIcon icon_ ;

    RInside R_;

    int totalRows_;
    int totalCols_;
    bool isHelp;
    bool edit_;

signals:


};

#endif // MAINWINDOW_H
